function Json(data)
{
	function Objeto(arreglo)
			{
				switch(arreglo.length)//segun el numero de elementos que contenga el arreglo se construira el objetazo
				{
					case 5:
						this.nom = arreglo[0];
						this.posc = arreglo[1];
						this.rend = arreglo[2];
						this.plus = arreglo[3];
						this.img = arreglo[4];
						this.ico = "null";
						this.nimg ="null";
						this.limg = "null";
						this.coorX = "null";
						this.coorY = "null";
						this.direc = "null";
						this.zona = "null";
						this.precio="null";
						this.mts = "null";
						break;
					case 7:
						this.nom = arreglo[0];
						this.posc = arreglo[1];
						this.rend = arreglo[2];
						this.plus = arreglo[3];
						this.img = arreglo[4];
						this.ico = arreglo[5];
						this.mts = arreglo[6];
						this.nimg ="null";
						this.limg = "null";
						this.coorX = "null";
						this.coorY = "null";
						this.direc = "null";
						this.zona = "null";
						this.precio="null";
						break;
					case 11:
						this.nom = arreglo[0];
						this.posc = arreglo[1];
						this.rend = arreglo[2];
						this.plus= arreglo[3];
						this.nimg = arreglo[4];
						this.limg = arreglo[5];
						this.coorX = arreglo[6];
						this.coorY = arreglo[7];
						this.direc = arreglo[8];
						this.zona = arreglo [9];
						this.precio = arreglo[10];
						this.img = "null";
						this.ico = "null";
						this.mts = "null";
						break;
					default ://si da el caso que mandes un objeto como parametro este vendra al default y se creara el objeto copia
						this.nom = arreglo.nom;
						this.posc = arreglo.posc;
						this.rend = arreglo.rend;
						this.plus = arreglo.plus;
						this.nimg = arreglo.nimg;
						this.limg = arreglo.limg;
						this.coorX = arreglo.coorX;
						this.coorY = arreglo.coorY;
						this.direc = arreglo.direc;
						this.zona = arreglo.zona;
						this.img = arreglo.img;
						this.ico= arreglo.ico;
						this.precio = arreglo.precio;
						this.mts = arreglo.mts;
					    break;
				}//fin del switch
			}//fin de la funcion

			this.RetornaJson=function(CadBusq)
			{
				var file =  data;
				var Document = file;//file contiene todo lo que hay dentro del documento
				var Nelem = Object.keys(Document[CadBusq]).length;/*Sacamos el numero de llaves que contine el objeto a buscar*/
				var StrngObj = JSON.stringify(Document[CadBusq]);//El objeto a buscar lo extraemos  y lo metemos a un string
				Document = JSON.parse(StrngObj);//pasamos el string a Json
				switch(Nelem)
				{
					case 5: var array=[Document["Nom"],Document["PosC"],Document["Rend"],Document["Plus"],Document["Img"]];
							var obj= new Objeto(array);
							return obj;
							break;
					case 7: var array=[Document["Nom"],Document["PosC"],Document["Rend"],Document["Plus"],Document["Img"],Document["Ico"],Document["Mts"]];
							var obj= new Objeto(array);
							return obj;
							break;
					case 11: var array=[Document["Nom"],Document["PosC"],Document["Rend"],Document["Plus"],Document["NImg"],Document["LImg"],Document["CoorX"],Document["CoorY"],Document["Dir"],Document["Zona"],Document["Precio"]];
							var obj= new Objeto(array);
							return obj;
							break;
				}//fin del switch
			}//fin de la funcion
}//Fin de la clase