//var ambiant = new Ambiant("Menu.txt");
var contadorSenales=0;
var contadorEntradas=0;
var senalActualizar;

function Opcion()
{
    this.id = 0;
    this.texto = "";
}

function Render(file)
{
  var this_ = this;
  function processData(Data)
  {
    this_.renders=Data.split('\n');
  	this_.Modelo = this_.renders[0].split('|');
  	this_.Fachada = this_.renders[1].split('|');
  	this_.Piedra = this_.renders[2].split('|');
  	this_.Color_Fa = this_.renders[3].split('|');
  	this_.Puert_int = this_.renders[4].split('|');
  	this_.Piso_S = this_.renders[5].split('|');
  	this_.Herreria = this_.renders[6].split('|');
  	this_.Mod_C = this_.renders[7].split('|');
  	this_.Mel_C = this_.renders[8].split('|');
  	this_.Cu_C = this_.renders[9].split('|');
  	this_.Mueble = this_.renders[10].split('|');
  	this_.Cancel = this_.renders[11].split('|');
  	this_.Piso_B = this_.renders[12].split('|');
  	this_.Rec_Reg = this_.renders[13].split('|');
  	this_.Mel_Clo = this_.renders[14].split('|');
  	this_.indices = this_.renders[15].split('|');
  	this_.llaves = this_.renders[16].split('|');
    this_.DosRec = this_.renders[17].split('|');
    this_.TresRec = this_.renders[18].split('|');
    this_.RG = this_.renders[19].split('|');
    this_.FR = this_.renders[20].split('|');
    this_.CrearDiccionario();
  }
  function load()
  {
      if(this.status === 200 )
      {
          // success!
          processData(this.responseText);
      }
      else
      {
          // something went wrong
          console.log('Algo salio mal');
      }
  };

  var archivo = "js/ambiant-2.0-js/" + file;
  var client = new XMLHttpRequest();
  client.onload = load;
  client.open("GET", archivo);
  client.send();


	this.Reg_Rend=function()
  {
		var list = {};
    list["PBaja"] = this_.PlantaPB;
    list["PAlta2"] = this_.PlantaPA2;
    list["PAlta3"] = this_.PlantaPA3;
    list["PRG"] = this_.PlantaRG;
    list["PFR"] = this_.PlantaFR;
    list["Fachada"] = this_.FA;
    list["Sala"] = this_.Sala;
    list["Cocina"] = this_.Cocina;
    list["Bano"] = this_.Bano;
    list["Closet"] = this_.Closet;
    list["Metros"] = this_.Metros;
    return list;
		//return list=[this_.PlantaPB,this_.PlantaPA2,this_.PlantaPA3,this_.PlantaRG,this_.PlantaFR,
      //this_.FA,this_.Sala,this_.Cocina,this_.Bano,this_.Closet];
	}

  this.Construir_Rend=function(Cadena){
    this_.PlantaPB="PLANTA-"+this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]+"-PB.png";
    this_.PlantaPA2="PLANTA-"+this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]+"-PA2.png";
    this_.PlantaPA3="PLANTA-"+this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]+"-PA3.png";
    this_.PlantaRG="PLANTA-"+this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]+"-RG.png";
    this_.PlantaFR="PLANTA-"+this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]+"-FR.png";
    this_.FA="FA-"+this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]+"-"+this_.Fachada[Number(Cadena[this_.diccionario["Fa"]])-1]+"-"+this_.Piedra[Number(Cadena[this_.diccionario["Pie"]])-1]+"-"+this_.Color_Fa[Number(Cadena[this_.diccionario["CoF"]])-1]+".jpg";
    this_.Sala= "SALA-"+this_.Puert_int[Number(Cadena[this_.diccionario["PInt"]])-1]+"-"+this_.Piso_S[Number(Cadena[this_.diccionario["PiS"]])-1]+"-"+this_.Herreria[Number(Cadena[this_.diccionario["Hrr"]])-1]+".jpg";
    if(this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]==="GY"){
          this_.Cocina= "COCINA-"+this_.Mod_C[0]+"-"+this_.Mel_C[Number(Cadena[this_.diccionario["MelC"]])-1]+"-"+this_.Cu_C[Number(Cadena[this_.diccionario["CuC"]])-1]+".jpg";
      }else {
            if (this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]=="DL"||this_.Modelo[Number(Cadena[this_.diccionario["Mod"]])-1]=="BT") {
                this_.Cocina= "COCINA-"+this_.Mod_C[1]+"-"+this_.Mel_C[Number(Cadena[this_.diccionario["MelC"]])-1]+"-"+this_.Cu_C[Number(Cadena[this_.diccionario["CuC"]])-1]+".jpg";
            }else{
                this_.Cocina= "COCINA-"+this_.Mod_C[2]+"-"+this_.Mel_C[Number(Cadena[this_.diccionario["MelC"]])-1]+"-"+this_.Cu_C[Number(Cadena[this_.diccionario["CuC"]])-1]+".jpg";
            }//fin else
      }//fin del else
      this_.Bano = "BANO-"+this_.Mueble[Number(Cadena[this_.diccionario["Mu"]])-1]+"-"+this_.Cancel[Number(Cadena[this_.diccionario["Can"]])-1]+"-"+this_.Piso_B[Number(Cadena[this_.diccionario["PiB"]])-1]+"-"+this_.Rec_Reg[Number(Cadena[this_.diccionario["Reg"]])-1]+".jpg";
      this_.Closet = "CLOSET-"+this_.Mel_Clo[Number(Cadena[this_.diccionario["Clo"]])-1]+".jpg";
      switch (Number(Cadena[2])-1) {
        case 0:
          switch (Number(Cadena[1])-1) {
            case 0:
              this_.Metros = this_.DosRec[0];
              break;
            case 1:
              this_.Metros = this_.DosRec[1];
              break;
            case 2:
              this_.Metros = this_.DosRec[2];
              break;
            case 3:
              this_.Metros = this_.DosRec[3];
              break;
            }
            break;
        case 1:
          switch (Number(Cadena[1])-1) {
            case 0:
              this_.Metros = this_.TresRec[0];
              break;
            case 1:
              this_.Metros = this_.TresRec[1];
              break;
            case 2:
              this_.Metros = this_.TresRec[2];
              break;
            case 3:
              this_.Metros = this_.TresRec[3];
              break;
            }
            break;
          case 2:
            switch (Number(Cadena[1])-1) {
              case 0:
                this_.Metros = this_.RG[0];
                break;
              case 1:
                this_.Metros = this_.RG[1];
                break;
              case 2:
                this_.Metros = this_.RG[2];
                break;
              case 3:
                this_.Metros = this_.RG[3];
                break;
              }
            break;
          case 3:
            switch (Number(Cadena[1])-1) {
              case 0:
                this_.Metros = this_.FR[0];
                break;
              case 1:
                this_.Metros = this_.FR[1];
                break;
              case 2:
                this_.Metros = this_.FR[2];
                break;
              case 3:
                this_.Metros = this_.FR[3];
                break;
              }
            break;
    }
	}//fin de la funcion Construir Render

	this.CrearDiccionario=function()
  {
		for (var i = 0; i < this_.llaves.length; i++) {
		    this_.diccionario[this_.llaves[i]]=this_.indices[i];
		}
	}//fin de la funcion que crea el diccionario

	this.Imprime=function()
  {
		console.log(this_.PlantaPB);
		console.log(this_.PlantaPA2);
		console.log(this_.PlantaPA3);
		console.log(this_.PlantaRG);
		console.log(this_.PlantaFR);
		console.log(this_.FA);
		console.log(this_.Sala);
		console.log(this_.Cocina);
		console.log(this_.Bano);
		console.log(this_.Closet);
	}
	//Variables
	this.diccionario = new Array();
  var PlantaPB;
  var PlantaPA2;
  var PlantaPA3;
  var PlantaRG;
  var PlantaFR;
  var FA;
  var Sala;
  var Cocina;
  var Bano;
  var Closet;
  var Metros;
  var renders;
	var Modelo;
	var Fachada;
	var Piedra;
	var Color_Fa;
	var Puert_int;
	var Piso_S;
	var Herreria;
	var Mod_C;
	var Mel_C;
	var Cu_C;
	var Mueble;
	var Cancel;
	var Piso_B;
	var Rec_Reg;
	var Mel_Clo;
  var DosRec;
  var TresRec;
  var RG;
  var FR;
	var indices;
	var llaves;
}//fin de la clase Render

function Ambiant(arch)
{
  var this_ = this;
  senalActualizar = new signals.Signal();
  senalActualizar.add(actualizaC_Global);
  this.menu = new Menu(0,0,arch);
  this.renders = new Render("Arreglos.txt");
  var LONGITUD_PATRON = 3;
  this.configuracionGlobal = "";

  this.inicializaC_Global = function()
  {
    var cG = "";
    for (var i = 0; i < this.menu.submenu.length; i++)
    {
      var configuracion = this.menu.submenu[i].configuracion;
      for (var j = 0; j < (configuracion.length+1)/(LONGITUD_PATRON+1); j++)
      {
        cG += configuracion[LONGITUD_PATRON-1+(j*(LONGITUD_PATRON+1))];
      }
    }
    this.configuracionGlobal = cG;
  }

  function actualizaC_Global(opc)
  {
    var i;
    var n1;
    var n2;
    var pos;
    var indiceJerarquia;
    var configuracion = "";
    var nuevaConfiguracion = "";

    n1 = Math.floor(opc / 100);
    i = opc % 100;
    n2 = Math.floor(i / 10);
    pos = 0;
    if (n2 === 0)
    {
      n2 = 10;
    }

    for (var i = 0; i < n1-1; i++)
    {
      pos += this_.menu.submenu[i].opciones.length;
    }
    indiceJerarquia = i;
    configuracion = this_.menu.submenu[indiceJerarquia].configuracion;
    nuevaConfiguracion = this_.configuracionGlobal;
    for (var i = 0; i < this_.menu.submenu[indiceJerarquia].opciones.length; i++)
    {
      nuevaConfiguracion = nuevaConfiguracion.substr(0,pos+i) + configuracion[LONGITUD_PATRON-1+(i*(LONGITUD_PATRON+1))]
                            + nuevaConfiguracion.substr(pos+i+1);
    }
    this_.configuracionGlobal = nuevaConfiguracion;
    this_.renders.Construir_Rend(nuevaConfiguracion);

  }

  /*function onEnd()
  {
    this.inicializaC_Global();
    renders.Const_Render(configuracionGlobal);
  }*/
}

function Menu(niv, ind, archivoM, archivoC)
{
   this.todo;
   var this_=this;
    /* Parámetros por default */
    this.nivel = typeof niv !== 'undefined' ? niv : 0;
    this.indice = typeof ind !== 'undefined' ? ind : 0;
    this.archivoMenu = typeof archivoM !== 'undefined' ? archivoM : "";
    this.archivoCombinaciones = typeof archivoC !== 'undefined' ? archivoC : "";
    /* Parámetros por default */

    this.opciones = []; //Arreglo de opciones
    this.submenu = []; //Arreglo de menus
    this.configuracion = "";
    this.configuraciones = [];
    this.configuracionesViables = [];
    this.patrones = [];
    this.re = new RegExp(""); //expresion regular
    this.tieneCandado = false;
    var LONGITUD_PATRON = 3;

    this.señal = new signals.Signal();
    this.señal.add(onEnd);

    function onEnd() {

      contadorSenales++;
      if(contadorSenales === contadorEntradas)
      {
        sigFinal.dispatch();

      }
    }

    this.leerCombinaciones = function(archivo)
    {
        var cadena = "";

        function processData(text)
        {

            var data = text
            for (var j = 0; j < data.length; j++)
            {
                if (data[j] === '\n')
                {
                    this_.configuraciones.push(cadena);
                    cadena = "";
                }
                else
                {
                    cadena += data[j];
                }
            }
            this_.configuracion = this_.configuraciones[0];
            this_.configuracionesViables = this_.configuraciones;
            this_.señal.dispatch();
        };


        function load()
        {
            if(this.status === 200 )
            {
                // success!
                processData(this.responseText);
            }
            else
            {
                // something went wrong
                console.log('Algo salio mal');
            }
        };

        archivo = "js/ambiant-2.0-js/" + archivo;
        var client = new XMLHttpRequest();
        client.onload = load;
        client.open("GET", archivo);
        client.send();
    };

    this.leerConfiguraciones = function(archivoPrincipal)
    {
        var cadena = "";
        var entradas = [];
        var lineIndex;
        function processData(text)
        {
            var data = text;
            lineIndex = 0;
            for (var i = 0; i < data.length; i++)
            {
                if (data[i] === '\n')
                {

                    entradas = cadena.split(",")
                    if (lineIndex === 0)
                    {
                        for (var j = 0; j < entradas.length; j++)
                        {
                            var opc = new Opcion();
                            opc.id = j + 1;
                            opc.texto = entradas[j];
                            this_.opciones.push(opc);
                        }
                    }
                    else
                    {
                        contadorEntradas++;
                        var m = new Menu(this_.nivel + 1, lineIndex, entradas[0], entradas[1]);
                        this_.submenu.push(m);

                    }
                    lineIndex++;
                    cadena = "";
                }
                else
                {
                    cadena += data[i];
                }
            }
            /*Aqui se manda la señal para poder usar el sistema*/



        };

        function end() {
          // body...

        }

        function load()
        {
            if(this.status == 200 )
            {
                // success!
                processData(this.responseText);
            }
            else
            {
                // something went wrong
                console.log('Algo salio mal');
            }
        };
        var archivo = "js/ambiant-2.0-js/" + archivoPrincipal;
        var client = new XMLHttpRequest();
        client.onload = load;
        client.onloadend = end;
        client.open("GET", archivo);
        client.send();
    };

    this.leerConfiguraciones2 = function(archivoMenu, archivoCombinaciones)
    {
        var cadena = "";
        var entradas = [];
        var subM = []; //arreglo de menus
        var lineIndex = 0;
        var erLeida = false;

        function processData(text)
        {
            var data = text;

            for (var i = 0; i < data.length; i++)
            {
                if (data[i] === '\n')
                {
                    if (!erLeida)
                    {
                        this_.re = new RegExp(cadena);
                        erLeida = true;
                    }
                    else
                    {
                        entradas = cadena.split(",");
                        entradas.splice(0,1);
                        if (lineIndex === 0)
                        {
                            for (var j = 0; j < entradas.length; j++)
                            {
                                var opc = new Opcion();
                                var newId;
                                if (j + 1 === 10)
                                {
                                  j = -1;
                                  newId = (Math.pow(10, this_.nivel) * this_.indice) + j + 1;
                                  j = 9;
                                }
                                else
                                {
                                    newId = (Math.pow(10, this_.nivel) * this_.indice) + j + 1;
                                }
                                opc.id = newId;
                                opc.texto = entradas[j];
                                this_.opciones.push(opc);
                            }
                        }
                        else
                        {
                            var menu = new Menu(this_.nivel + 1, lineIndex);
                            if (entradas[entradas.length - 1] === "*")
                            {
                                menu.tieneCandado = true;
                                entradas.splice(entradas.length-1, 1);
                            }
                            this_.patrones.push(entradas[entradas.length - 1]);
                            entradas.splice(entradas.length-1, 1);
                            var ops = [];
                            for (j = 0; j < entradas.length; j++)
                            {
                                var opci = new Opcion();
                                if (lineIndex === 10)
                                {
                                  opci.id = (Math.pow(10, menu.nivel) * this_.indice) + (Math.pow(10, this_.nivel) * 0) + j + 1;
                                }
                                else
                                {
                                    opci.id = (Math.pow(10, menu.nivel) * this_.indice) + (Math.pow(10, this_.nivel) * lineIndex) + j + 1;
                                }
                                opci.texto = entradas[j];
                                ops.push(opci);
                            }
                            menu.opciones = ops;
                            subM.push(menu);
                        }
                        lineIndex++;
                    }

                    cadena = "";
                }
                else
                {
                    cadena += data[i];
                }
            }
            //this_.señal.dispatch();
            this_.submenu = subM;
            this_.leerCombinaciones(archivoCombinaciones);


        };


        function load()
        {
            if(this.status == 200 )
            {
                // success!
                processData(this.responseText);
            }
            else
            {
                // something went wrong
                console.log('Algo salio mal leyendo las combinaciones');
            }
        };
        var archivo = "js/ambiant-2.0-js/" + archivoMenu;
        var client = new XMLHttpRequest();
        client.onload = load;
        client.open("GET", archivo);
        client.send();
    };

    if (this.archivoMenu === "" && this.archivoCombinaciones === "")
    {
    }
    else if (this.archivoCombinaciones === "")
    {
        this.leerConfiguraciones(this.archivoMenu);
    }
    else
    {
        this.leerConfiguraciones2(this.archivoMenu, this.archivoCombinaciones);
    }

    this.validaElegible = function(aux, nivel, idex, configuraciones)
    {
        if (idex >= aux.length)
        {
            return aux;
        }
        else
        {
            var pat = aux.substr(idex-1, LONGITUD_PATRON-1);
            var original = aux.substr((nivel-1) * (LONGITUD_PATRON + 1), LONGITUD_PATRON);
            if (configuraciones.indexOf(aux) >= 0)
            {
                return aux;
            }
            else
            {
                for (var j = 0; j < 9; j++)/*5*/
                {
                    var searchValue = aux.substr(idex-1, LONGITUD_PATRON);
                    var newValue = pat + (j+1).toString();
                    aux = aux.replace(searchValue, newValue);

                    if (configuraciones.indexOf(aux) >= 0)
                    {
                        return aux;
                    }
                    aux = this.validaElegible(aux, nivel + 1, idex + LONGITUD_PATRON + 1, configuraciones);
                    if (configuraciones.indexOf(aux) >= 0)
                    {
                        return aux;
                    }
                }
                searchValue = aux.substr(idex-1, LONGITUD_PATRON);
                aux = aux.replace(searchValue, original);
                return aux;
            }
        }
    };

    this.validaOpciones = function(configuraciones, configuracion, patron, ops, candado, niv)
    {
        var aux; //string
        var result; //lista de opciones
        result = []//ops;

        for (var i = 0; i < ops.length; i++)
        {
          var o = new Opcion()
          o.id = ops[i].id
          o.texto = ops[i].texto
          result.push(o)
        }

        for (var i = 0; i < result.length; i++)
        {
            aux = configuracion;

            var searchIndex = aux.search(patron.substr(0,LONGITUD_PATRON-1));
            var searchValue = patron.substr(0,LONGITUD_PATRON-1) + aux[searchIndex+LONGITUD_PATRON-1];
            var newValue = patron.substr(0,LONGITUD_PATRON-1) + (i+1).toString();
            aux = aux.replace(searchValue, newValue);

            if (configuraciones.indexOf(aux) < 0)
            {
                if (candado)
                {
                    var idex = aux.search(patron.substr(0,LONGITUD_PATRON-1)) + LONGITUD_PATRON-1 + LONGITUD_PATRON;
                    aux = this.validaElegible(aux, niv + 1, idex, configuraciones)
                    if (configuraciones.indexOf(aux) < 0)
                    {
                        var op = new Opcion();
                        op = result[i];
                        op.texto = op.texto + "*";
                        result[i] = op;
                    }
                }
                else
                {
                    var opi = new Opcion();
                    opi = result[i];
                    opi.texto = opi.texto + "*";
                    result[i] = opi;
                }
            }
        }
        return result;
    };

    this.actualiza = function(confs, confsViables, config, pats, patron, candado, niv, opc, indicePatron)
    {
        var aux = config
        if (candado)
        {
            var searchIndex = config.search(patron.substr(0,LONGITUD_PATRON-1));
            var searchValue = patron.substr(0,LONGITUD_PATRON-1) + config[searchIndex+LONGITUD_PATRON-1];
            var newValue = patron.substr(0,LONGITUD_PATRON-1) + opc.toString();
            config = config.replace(searchValue, newValue);

            if (confsViables.indexOf(config) < 0)
            {
                config = this.aplicaCandado(confs, config, pats, niv + 1, indicePatron + 1);
                if (confs.indexOf(config) < 0)
                {
                    config = aux;
                }
            }
        }
        else
        {
            var searchIndexi = aux.search(patron.substr(0,LONGITUD_PATRON-1));
            var searchValuei = patron.substr(0,LONGITUD_PATRON-1) + aux[searchIndexi+LONGITUD_PATRON-1];
            var newValuei = patron.substr(0,LONGITUD_PATRON-1) + opc.toString();
            aux = aux.replace(searchValuei, newValuei);

            if (confs.indexOf(aux) >= 0)
            {
                var auxConfViables = [];
                var auxRE = new RegExp(newValue);
                for (var i = 0; i < confs.length; i++)
                {
                    if (auxRE.test(confs[i]))
                    {
                        auxConfViables.push(confs[i]);
                    }
                }

                confsViables = auxConfViables;
                config = config.replace(searchValuei, newValuei);
            }
        }
        return [confsViables, config];
    };

    this.aplicaCandado = function(confs, config, pats, niv, indicePatron)
    {
        var aux = config;
        if (niv > pats.length)
        {
            return config;
        }
        else
        {
            var original = aux.substr((indicePatron) * (LONGITUD_PATRON + 1), LONGITUD_PATRON);

            for (var i = 0; i < pats.length; i++)/*pats.length*/
            {
                var searchIndex = aux.search(pats[indicePatron].substr(0,LONGITUD_PATRON-1));
                var searchValue = pats[indicePatron].substr(0,LONGITUD_PATRON-1) + aux[searchIndex+LONGITUD_PATRON-1];
                var newValue = pats[indicePatron].substr(0,LONGITUD_PATRON-1) + (i+1).toString();

                if (aux.search(new RegExp(pats[indicePatron])) === -1)
                {
                  // console.log("Cortando por el bien del mundo")
                  break;
                }

                aux = aux.replace(searchValue, newValue);
                if (confs.indexOf(aux) >= 0)
                {
                    config = aux;
                    return config;
                }
                aux = this.aplicaCandado(confs, aux, pats, niv + 1, indicePatron + 1);
                if (confs.indexOf(aux) >= 0)
                {
                    config = aux;
                    return config;
                }
            }
            searchIndex = config.search(pats[indicePatron].substr(0,LONGITUD_PATRON-1));
            searchValue = pats[indicePatron].substr(0,LONGITUD_PATRON-1) + config[searchIndex+LONGITUD_PATRON-1];
            config = config.replace(searchValue, original);
            return config;
        }
    };

    this.aplicaOpcion = function(opcion)
    {
        var opcNivel3;
        var opcNivel2;
        var opcNivel1;
        var aux;
        var result = []; //lista de opciones

        opcNivel3 = Math.floor(opcion / 100);
        aux = opcion % 100;
        opcNivel2 = Math.floor(aux / 10);
        opcNivel1 = aux % 10;



        if (opcNivel3 > 0)
        {
          if (opcNivel2 === 0)
          {
            opcNivel2 = 10;
          }
            var confsV = this.submenu[opcNivel3-1].configuracionesViables;
            var config = this.submenu[opcNivel3-1].configuracion;
            var res = this.actualiza(this.submenu[opcNivel3-1].configuraciones,
                      confsV,
                      config,
                      this.submenu[opcNivel3-1].patrones,
                      this.submenu[opcNivel3-1].patrones[opcNivel2-1],
                      this.submenu[opcNivel3-1].submenu[opcNivel2-1].tieneCandado,
                      opcNivel2,
                      opcNivel1,
                      opcNivel2-1);
            this.submenu[opcNivel3-1].configuracionesViables = res[0];
            this.submenu[opcNivel3-1].configuracion = res[1];
            senalActualizar.dispatch(opcion)
        }
        else if (opcNivel2 > 0)
        {
          if (opcNivel1 === 0)
          {
            opcNivel1 = 10;
          }
            result = this.submenu[opcNivel2-1].submenu[opcNivel1-1].opciones;
            result = this.validaOpciones(this.submenu[opcNivel2-1].configuraciones,
                                    this.submenu[opcNivel2-1].configuracion,
                                    this.submenu[opcNivel2-1].patrones[opcNivel1-1],
                                    result,
                                    this.submenu[opcNivel2-1].submenu[opcNivel1-1].tieneCandado,
                                    opcNivel1);
        }
        else
        {

            result = this.submenu[opcNivel1-1].opciones;
        }
        return result;
    };

    this.obtenerConfiguraciones = function()
    {
        var resultado = [];
        for (var i = 0; i < this.submenu.length; i++)
        {
            resultado.push(this.submenu[i].configuracion);
        }
        return resultado;
    }
}
/*
var menu = new Menu(0, 0, "Menu.txt");
while(menu.submenu.length === 0)
    //console.log("A");

var opciones = [];
opciones = menu.aplicaOpcion(7);
console.log("opciones.size() = " + opciones.length);
for (var qq = 0; qq < opciones.length; qq++)
{
    console.log(opciones[qq].id + ". " + opciones[qq].texto);
}

var configuraciones = [];
configuraciones = menu.obtenerConfiguraciones();
console.log("configuraciones.length: " + configuraciones.length);
for (var ii = 0; ii < configuraciones.length; ii++)
{
    console.log(configuraciones[ii]);
}
*/
