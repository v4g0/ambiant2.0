	var topMarker;
	var leftMarker;
	var clicked = false;
	var id;
	var slider;
	var clicked_first = false;
	var marker_id_anterior = 0;
	var markerClick = 0;
	function CustomMarker(latlng, map, args) {
		this.latlng = latlng;
		this.args = args;
		this.setMap(map);



	}

	CustomMarker.prototype = new google.maps.OverlayView();

	CustomMarker.prototype.draw = function() {

		var self = this;

		var div = this.div;

		if (!div) {

			div = this.div = document.createElement('div');

			if(self.args.marker_id == 112 || self.args.marker_id == 113)
			{
				div.className = 'markerDown';
			}
			else
				div.className = 'marker';


			var img = document.createElement('img');
			img.src = this.args.marker_url; //"img/prueba.jpg";
			img.style.width = '100%';
			img.style.height = '100%';
			img.style.position = 'absolute';
			// img.style.left = '35px';
			// img.style.top = '5px';
			img.style.margin = 'auto';

			div.appendChild(img);

			if (typeof(self.args.marker_id) !== 'undefined') {
				div.dataset.marker_id = self.args.marker_id;
			}

			google.maps.event.addDomListener(div, "click", function(event) {

				ambiant.menu.aplicaOpcion(div.dataset.marker_id);
				clicked_first = true;
				var a = $( ".markerClicked" ).get();
				var b = $( ".markerClickedDown" ).get();
				markerClicked = div.dataset.marker_id;
				for( var i = 0; i < a.length; i++ )
				{
					// jQuery(a[i]).css( "-webkit-filter", "grayscale(1)");

						a[i].className = 'marker';
					// $(this).removeClass("g");
				}

				for( var i = 0; i < b.length; i++ )
				{
					// jQuery(a[i]).css( "-webkit-filter", "grayscale(1)");

						b[i].className = 'markerDown';
					// $(this).removeClass("g");
				}
				// console.log(marker.dataset.marker_id);
				//Cuando se preciona un marker se verifica si ya fue seleccionado anteriormente y de no ser asi se cargar el menu con los datos del fraccionamiento//
				if(clicked===false)
				{
					// console.log('click');
					setMenuInfo(div.dataset.marker_id);
					clicked=true;
					marker= div;
					if(self.args.marker_id == 112 || self.args.marker_id == 113)
						div.className='markerClickedDown';
					else
						div.className='markerClicked';
					showTerrenoInfo();
					hideSelecciona();

				}
				// else if (marker.dataset.marker_id !== div.dataset.marker_id)
				// {
				// 	setMenuInfo(div.dataset.marker_id);
				//
				// 	clicked=true;
				// 	marker.className='marker';
				// 	marker= div;
				// 	div.className='markerClicked';
				// }
				else
				{
					showTerrenoInfo();
					hideSelecciona();

					// if(slider)
					// {
					// 	console.log('existe');
					// 	slider.remove();
					// }
					setMenuInfo(div.dataset.marker_id);
					if(self.args.marker_id == 112 || self.args.marker_id == 113)
						div.className='markerClickedDown';
					else
						div.className='markerClicked';
					clicked=true;
					id=0;
					// div.className='marker';

				}
				jQuery('.marker').each(function () {

						if ( jQuery(this).attr("data-marker_id") !== div.dataset.marker_id ){
							jQuery(this).css( "-webkit-filter", "grayscale(1)");
							jQuery(this).css( "-moz-filter", "grayscale(1)");
							jQuery(this).css( "-ms-filter", "grayscale(1)");
							jQuery(this).css( "-o-filter:", "grayscale(1)");
							jQuery(this).css( "filter", "grayscale(1)");
							// jQuery(this).addClass('escalaGrises');
							// console.log( jQuery(this).toggleClass('special') );

							// jQuery(this).removeClass('g')
						}
						else
						{
							jQuery(this).css( "-webkit-filter", "grayscale(0)");
							jQuery(this).css( "-moz-filter", "grayscale(0)");
							jQuery(this).css( "-ms-filter", "grayscale(0)");
							jQuery(this).css( "-o-filter:", "grayscale(0)");
							jQuery(this).css( "filter", "grayscale(0)");
						}
						// console.log(jQuery(this));
				});

				jQuery('.markerDown').each(function () {

						if ( jQuery(this).attr("data-marker_id") !== div.dataset.marker_id ){
							jQuery(this).css( "-webkit-filter", "grayscale(1)");
							jQuery(this).css( "-moz-filter", "grayscale(1)");
							jQuery(this).css( "-ms-filter", "grayscale(1)");
							jQuery(this).css( "-o-filter:", "grayscale(1)");
							jQuery(this).css( "filter", "grayscale(1)");
							// jQuery(this).addClass('escalaGrises');
							// console.log( jQuery(this).toggleClass('special') );

							// jQuery(this).removeClass('g')
						}
						else
						{
							jQuery(this).css( "-webkit-filter", "grayscale(0)");
							jQuery(this).css( "-moz-filter", "grayscale(0)");
							jQuery(this).css( "-ms-filter", "grayscale(0)");
							jQuery(this).css( "-o-filter:", "grayscale(0)");
							jQuery(this).css( "filter", "grayscale(0)");
						}
						// console.log(jQuery(this));
				});
				jQuery('.markerClicked').each(function () {
						//console.log('maker_2');
						if ( jQuery(this).attr("data-marker_id") === div.dataset.marker_id ){
							jQuery(this).css( "-webkit-filter", "grayscale(0)");
							jQuery(this).css( "-moz-filter", "grayscale(0)");
							jQuery(this).css( "-ms-filter", "grayscale(0)");
							jQuery(this).css( "-o-filter:", "grayscale(0)");
							jQuery(this).css( "filter", "grayscale(0)");

							//console.log('maker_2.1');
							// jQuery(this).addClass('escalaGrises');
							// console.log( jQuery(this).toggleClass('special') );

							// jQuery(this).addClass('g');
						}
						else
						{
							// jQuery(this).addClass('g');
							jQuery(this).css( "-webkit-filter", "grayscale(1)");
							jQuery(this).css( "-moz-filter", "grayscale(1)");
							jQuery(this).css( "-ms-filter", "grayscale(1)");
							jQuery(this).css( "-o-filter:", "grayscale(1)");
							jQuery(this).css( "filter", "grayscale(1)");
						}
						//console.log(jQuery(this));
				});

				jQuery('.markerClickedDown').each(function () {
						//console.log('maker_2');
						if ( jQuery(this).attr("data-marker_id") === div.dataset.marker_id ){
							jQuery(this).css( "-webkit-filter", "grayscale(0)");
							jQuery(this).css( "-moz-filter", "grayscale(0)");
							jQuery(this).css( "-ms-filter", "grayscale(0)");
							jQuery(this).css( "-o-filter:", "grayscale(0)");
							jQuery(this).css( "filter", "grayscale(0)");

							//console.log('maker_2.1');
							// jQuery(this).addClass('escalaGrises');
							// console.log( jQuery(this).toggleClass('special') );

							// jQuery(this).addClass('g');
						}
						else
						{
							// jQuery(this).addClass('g');
							jQuery(this).css( "-webkit-filter", "grayscale(1)");
							jQuery(this).css( "-moz-filter", "grayscale(1)");
							jQuery(this).css( "-ms-filter", "grayscale(1)");
							jQuery(this).css( "-o-filter:", "grayscale(1)");
							jQuery(this).css( "filter", "grayscale(1)");
						}
						//console.log(jQuery(this));
				});

				// google.maps.event.trigger(self, "click");
			});

			google.maps.event.addDomListener(div, "mouseover", function(event) {
				//console.log('aqui');
				jQuery('.marker').each(function () {
					if ( jQuery(this).attr("data-marker_id") !== div.dataset.marker_id )
					{
						jQuery(this).css( "-webkit-filter", "grayscale(1)");
						jQuery(this).css( "-moz-filter", "grayscale(1)");
						jQuery(this).css( "-ms-filter", "grayscale(1)");
						jQuery(this).css( "-o-filter:", "grayscale(1)");
						jQuery(this).css( "filter", "grayscale(1)");
						// $(this).addClass("g");
					}
					 else {
						 jQuery(this).css( "-webkit-filter", "grayscale(0)");
						 jQuery(this).css( "-moz-filter", "grayscale(0)");
						 jQuery(this).css( "-ms-filter", "grayscale(0)");
						 jQuery(this).css( "-o-filter:", "grayscale(0)");
						 jQuery(this).css( "filter", "grayscale(0)");

						//  $(this).removeClass("g");

					 }
				});

				jQuery('.markerDown').each(function () {
					if ( jQuery(this).attr("data-marker_id") !== div.dataset.marker_id )
					{
						jQuery(this).css( "-webkit-filter", "grayscale(1)");
						jQuery(this).css( "-moz-filter", "grayscale(1)");
						jQuery(this).css( "-ms-filter", "grayscale(1)");
						jQuery(this).css( "-o-filter:", "grayscale(1)");
						jQuery(this).css( "filter", "grayscale(1)");
						// $(this).addClass("g");
					}
					 else {
						 jQuery(this).css( "-webkit-filter", "grayscale(0)");
						 jQuery(this).css( "-moz-filter", "grayscale(0)");
						 jQuery(this).css( "-ms-filter", "grayscale(0)");
						 jQuery(this).css( "-o-filter:", "grayscale(0)");
						 jQuery(this).css( "filter", "grayscale(0)");

						//  $(this).removeClass("g");

					 }
				});

				jQuery('.markerClicked').each(function () {
					// console.log("clicked");
					if ( jQuery(this).attr("data-marker_id") === div.dataset.marker_id )
					{
						jQuery(this).css( "-webkit-filter", "grayscale(0)");
						jQuery(this).css( "-moz-filter", "grayscale(0)");
						jQuery(this).css( "-ms-filter", "grayscale(0)");
						jQuery(this).css( "-o-filter:", "grayscale(0)");
						jQuery(this).css( "filter", "grayscale(0)");
						// $(this).removeClass("g");
						// console.log("clicked 2");
					}
					else {
						jQuery(this).css( "-webkit-filter", "grayscale(1)");
						jQuery(this).css( "-moz-filter", "grayscale(1)");
						jQuery(this).css( "-ms-filter", "grayscale(1)");
						jQuery(this).css( "-o-filter:", "grayscale(1)");
						jQuery(this).css( "filter", "grayscale(1)");
						// $(this).addClass("g");
					}
				});

				jQuery('.markerClickedDown').each(function () {
					// console.log("clicked");
					if ( jQuery(this).attr("data-marker_id") === div.dataset.marker_id )
					{
						jQuery(this).css( "-webkit-filter", "grayscale(0)");
						jQuery(this).css( "-moz-filter", "grayscale(0)");
						jQuery(this).css( "-ms-filter", "grayscale(0)");
						jQuery(this).css( "-o-filter:", "grayscale(0)");
						jQuery(this).css( "filter", "grayscale(0)");
						// $(this).removeClass("g");
						// console.log("clicked 2");
					}
					else {
						jQuery(this).css( "-webkit-filter", "grayscale(1)");
						jQuery(this).css( "-moz-filter", "grayscale(1)");
						jQuery(this).css( "-ms-filter", "grayscale(1)");
						jQuery(this).css( "-o-filter:", "grayscale(1)");
						jQuery(this).css( "filter", "grayscale(1)");
						// $(this).addClass("g");
					}
				});



				// google.maps.event.trigger(self, "mouseover");
			});
				google.maps.event.addDomListener(div, "mouseout", function(event) {
					//console.log('saliendo');
					jQuery('.marker').each(function () {
						if ( jQuery(this).attr("data-marker_id") !== div.dataset.marker_id )
						{
							if ( clicked == true )
							{
								jQuery(this).css( "-webkit-filter", "grayscale(1)");
	 						 jQuery(this).css( "-moz-filter", "grayscale(1)");
	 						 jQuery(this).css( "-ms-filter", "grayscale(1)");
	 						 jQuery(this).css( "-o-filter:", "grayscale(1)");
	 						 jQuery(this).css( "filter", "grayscale(1)");
							}
							else{
								jQuery(this).css( "-webkit-filter", "grayscale(0)");
	 						 jQuery(this).css( "-moz-filter", "grayscale(0)");
	 						 jQuery(this).css( "-ms-filter", "grayscale(0)");
	 						 jQuery(this).css( "-o-filter:", "grayscale(0)");
	 						 jQuery(this).css( "filter", "grayscale(0)");
							}
							// $(this).addClass("g");
						}
						 else {
							 if ( clicked == true  )
							 {
								 jQuery(this).css( "-webkit-filter", "grayscale(1)");
								 jQuery(this).css( "-moz-filter", "grayscale(1)");
								 jQuery(this).css( "-ms-filter", "grayscale(1)");
								 jQuery(this).css( "-o-filter:", "grayscale(1)");
								 jQuery(this).css( "filter", "grayscale(1)");
							 }
							 else{
								 jQuery(this).css( "-webkit-filter", "grayscale(0)");
								 jQuery(this).css( "-moz-filter", "grayscale(0)");
								 jQuery(this).css( "-ms-filter", "grayscale(0)");
								 jQuery(this).css( "-o-filter:", "grayscale(0)");
								 jQuery(this).css( "filter", "grayscale(0)");
							 }
							//  $(this).addClass("g");

						 }
					});

					jQuery('.markerDown').each(function () {
						if ( jQuery(this).attr("data-marker_id") !== div.dataset.marker_id )
						{
							if ( clicked == true )
							{
								jQuery(this).css( "-webkit-filter", "grayscale(1)");
							 jQuery(this).css( "-moz-filter", "grayscale(1)");
							 jQuery(this).css( "-ms-filter", "grayscale(1)");
							 jQuery(this).css( "-o-filter:", "grayscale(1)");
							 jQuery(this).css( "filter", "grayscale(1)");
							}
							else{
								jQuery(this).css( "-webkit-filter", "grayscale(0)");
							 jQuery(this).css( "-moz-filter", "grayscale(0)");
							 jQuery(this).css( "-ms-filter", "grayscale(0)");
							 jQuery(this).css( "-o-filter:", "grayscale(0)");
							 jQuery(this).css( "filter", "grayscale(0)");
							}
							// $(this).addClass("g");
						}
						 else {
							 if ( clicked == true  )
							 {
								 jQuery(this).css( "-webkit-filter", "grayscale(1)");
								 jQuery(this).css( "-moz-filter", "grayscale(1)");
								 jQuery(this).css( "-ms-filter", "grayscale(1)");
								 jQuery(this).css( "-o-filter:", "grayscale(1)");
								 jQuery(this).css( "filter", "grayscale(1)");
							 }
							 else{
								 jQuery(this).css( "-webkit-filter", "grayscale(0)");
								 jQuery(this).css( "-moz-filter", "grayscale(0)");
								 jQuery(this).css( "-ms-filter", "grayscale(0)");
								 jQuery(this).css( "-o-filter:", "grayscale(0)");
								 jQuery(this).css( "filter", "grayscale(0)");
							 }
							//  $(this).addClass("g");

						 }
					});

					jQuery('.markerClicked').each(function () {
						// console.log("clicked");
						if ( jQuery(this).attr("data-marker_id") === div.dataset.marker_id )
						{

							if ( clicked == true )
							{
								jQuery(this).css( "-webkit-filter", "grayscale(0)");
	 						 jQuery(this).css( "-moz-filter", "grayscale(0)");
	 						 jQuery(this).css( "-ms-filter", "grayscale(0)");
	 						 jQuery(this).css( "-o-filter:", "grayscale(0)");
	 						 jQuery(this).css( "filter", "grayscale(0)");
							}
							else
							{
								jQuery(this).css( "-webkit-filter", "grayscale(1)");
	 						 jQuery(this).css( "-moz-filter", "grayscale(1)");
	 						 jQuery(this).css( "-ms-filter", "grayscale(1)");
	 						 jQuery(this).css( "-o-filter:", "grayscale(1)");
	 						 jQuery(this).css( "filter", "grayscale(1)");
							}
							// $(this).addClass("g");
							// console.log("clicked 2");
						}
						else {
							if ( clicked == true )
							{
								jQuery(this).css( "-webkit-filter", "grayscale(0)");
	 						 jQuery(this).css( "-moz-filter", "grayscale(0)");
	 						 jQuery(this).css( "-ms-filter", "grayscale(0)");
	 						 jQuery(this).css( "-o-filter:", "grayscale(0)");
	 						 jQuery(this).css( "filter", "grayscale(0)");
						 }else{
							 jQuery(this).css( "-webkit-filter", "grayscale(1)");
							jQuery(this).css( "-moz-filter", "grayscale(1)");
							jQuery(this).css( "-ms-filter", "grayscale(1)");
							jQuery(this).css( "-o-filter:", "grayscale(1)");
							jQuery(this).css( "filter", "grayscale(1)");
							}// $(this).addClass("g");
						}
					});

					jQuery('.markerClickedDown').each(function () {
						// console.log("clicked");
						if ( jQuery(this).attr("data-marker_id") === div.dataset.marker_id )
						{

							if ( clicked == true )
							{
								jQuery(this).css( "-webkit-filter", "grayscale(0)");
							 jQuery(this).css( "-moz-filter", "grayscale(0)");
							 jQuery(this).css( "-ms-filter", "grayscale(0)");
							 jQuery(this).css( "-o-filter:", "grayscale(0)");
							 jQuery(this).css( "filter", "grayscale(0)");
							}
							else
							{
								jQuery(this).css( "-webkit-filter", "grayscale(1)");
							 jQuery(this).css( "-moz-filter", "grayscale(1)");
							 jQuery(this).css( "-ms-filter", "grayscale(1)");
							 jQuery(this).css( "-o-filter:", "grayscale(1)");
							 jQuery(this).css( "filter", "grayscale(1)");
							}
							// $(this).addClass("g");
							// console.log("clicked 2");
						}
						else {
							if ( clicked == true )
							{
								jQuery(this).css( "-webkit-filter", "grayscale(0)");
							 jQuery(this).css( "-moz-filter", "grayscale(0)");
							 jQuery(this).css( "-ms-filter", "grayscale(0)");
							 jQuery(this).css( "-o-filter:", "grayscale(0)");
							 jQuery(this).css( "filter", "grayscale(0)");
						 }else{
							 jQuery(this).css( "-webkit-filter", "grayscale(1)");
							jQuery(this).css( "-moz-filter", "grayscale(1)");
							jQuery(this).css( "-ms-filter", "grayscale(1)");
							jQuery(this).css( "-o-filter:", "grayscale(1)");
							jQuery(this).css( "filter", "grayscale(1)");
							}// $(this).addClass("g");
						}
					});
					// google.maps.event.trigger(self, "mouseout");
				});
			var panes = this.getPanes();
			panes.overlayImage.appendChild(div);
		}

		var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

		leftMarker = point.x;
		topMarker = point.y;

		if (point) {
			//Se colocan los marker en el mapa pero se separan un poco unos de otros segun el id
			if(self.args.marker_id == 112 || self.args.marker_id == 113)
			{
				div.style.top = (point.y + 10) + 'px';
				if(self.args.marker_id = 113)
					div.style.left = (point.x - 70) + 'px';
				else
					div.style.left = (point.x - 80) + 'px';
			}
			else if(self.args.marker_id == 111 )
			{
				div.style.top = (point.y - 90) + 'px';
				div.style.left = (point.x - 65) + 'px';
			}
			else
			{


				div.style.left = (point.x - 70) + 'px';
				div.style.top = (point.y - 100) + 'px';
			}
		}
	};

	CustomMarker.prototype.remove = function() {
		if (this.div) {
			this.div.parentNode.removeChild(this.div);
			this.div = null;
		}
	};

	CustomMarker.prototype.getPosition = function() {
		return this.latlng;
	};

	function setMenuInfo(id) {
		  var obj_json = json_ambiant.RetornaJson( id );

			//console.log( obj_json );
			setInfo(id, obj_json.nom, "Desde "+obj_json.precio, obj_json['direc']);
			var lista_img = [];
			for (var key in obj_json.limg) {

		    lista_img.push( "images/fracc/" + obj_json.limg[key] );
			}
			showInfo(lista_img);
	}

	function hideSelecciona(){
		 $('#seleccionaTerreno').css('visibility', 'hidden');

	}

	function showSelecciona(){
		 $('#seleccionaTerreno').css('visibility', 'visible');

	}
	function showTerrenoInfo(){
		$('#informacionTerreno').show();
	}
  function hideInfo() {

		$('#informacionTerreno').hide();
  }


	function showInfo(l)
	{

		if(slider)
		{
			// console.log('existe');
			slider.remove();
		}
		slider = new Slider();
		slider.initialize(l);

	}

	function setInfo(id, nom, dir, zona) {
		$('#infoTerreno h2').text(nom);
		// $('#infoTerreno h1').remove();
		$('#infoTerreno h3').text(dir);
		$('#infoTerreno h4').text(zona);
	}

	function removeInfo() {
		$('#infoText h1').text("Selecciona un fraccionamiento!");
		$('#infoText p').text("");
	}
