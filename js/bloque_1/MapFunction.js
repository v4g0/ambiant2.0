var leftContainerPG=false;
var sigFinal;
var json_ambiant;
var renders;
var ambiant;
var myMap;


// lecura del archivo-diccionario de ivan
$.getJSON("Diccionario.json", function(data) {
  json_ambiant = new Json(data);
});

$(document).ready(function(){
  if (!((screen.width>=1024) && (screen.height>=648)))
  {
    window.location="salva_pantallas.html";
  }
  if((screen.width>=1280) && (screen.height>=1024))
  {
    $('.leftMenu').removeClass('leftMenuPC');
    $('.leftMenu').addClass('leftMenuPG');
    $('.leftContainer').removeClass('leftContainerPC');
    $('.leftContainer').addClass('leftContainerPG');
    $('.leftContainerExtended').removeClass('leftContainerPC');
    $('.leftContainerExtended').addClass('leftContainerPG');
    $('.keyMenu').removeClass('keyMenuPC');
    $('.keyMenu').addClass('keyMenuPG');
    $('.keyMenuTwoPC').addClass('keyMenuTwoPG');
    $('.keyMenuTwoPC').removeClass('keyMenuTwoPC');
    $('.keyMenuTooPC').addClass('keyMenuTooPG');
    $('.keyMenuTooPC').removeClass('keyMenuTooPC');
    $('.keyMenuLlave').removeClass('keyMenuLlavePC');
    $('.keyMenuLlave').addClass('keyMenuLlavePG');
    $('.keyItemContainer').removeClass('keyItemContainerPC');
    $('.keyItemContainer').addClass('keyItemContainerPG');
    $('#sliderSec3').removeClass('sliderSec3PC');
    $('#sliderSec3').addClass('sliderSec3PG');

    $('#sliderSec2').css('top',"20%");
    $('#sliderSec3').css('top',"90%");

    leftContainerPG = true;



  }
  else
  {
    leftContainerPG = false;
  }

  leerInstructivoData();
  inicializarMenu();


  $.getJSON("Diccionario.json", function(data) {
    json_ambiant = new Json(data);
  });

  sigFinal = new signals.Signal();
  sigFinal.add(onEnd);

  ambiant = new Ambiant("Menu.txt")
  //var menu = new Menu(0, 0, "Menu.txt");


  function onEnd() {
    //ambiant.renders.CrearDiccionario();
    ambiant.inicializaC_Global();
    ambiant.renders.Construir_Rend(ambiant.configuracionGlobal);

    /*
    CADA QUE SE APLIQUE UNA OPCIÓN FINAL LLAMAR:
    ambiant.menu.aplicaOpcion(733);
    var Ren = ambiant.renders.Reg_Rend();
    console.log(Ren["Bano"]);

    El orden en que regresa es el siguiente:
    "PBaja"
    "PAlta2"
    "PAlta3"
    "PRG"
    "PFR"
    "Fachada"
    "Sala"
    "Cocina"
    "Bano"
    "Closet"

    Iniciando en el índice 0
    */

    $('#seleccionaTerreno').show();
    var opciones = [];
    opciones = ambiant.menu.aplicaOpcion(11);
    //112 y 113
    for (var qq = 0; qq < opciones.length; qq++)
    {

        var objeto_frac = json_ambiant.RetornaJson( opciones[qq].id );
        var myLatlng = new google.maps.LatLng( objeto_frac.coorX, objeto_frac.coorY);
        overlay = new CustomMarker(
          myLatlng,
          myMap,
          {
            marker_id: opciones[qq].id,
            marker_url: "images/fracc/" + objeto_frac.rend
          }
        );
    }

    // cargarPlantas();
    // var can = document.getElementById('canvas1');
    // var ctx = can.getContext('2d');
    //
    // ctx.canvas.width = window.innerWidth;
    // ctx.canvas.height = window.innerHeight;
    // ctx.fillStyle = "rgba(0,0,0,0.2)";
    // ctx.beginPath();
    // ctx.rect(0,0,window.innerWidth,window.innerHeight);
    //
    // ctx.arc(800,500,50,0,2*Math.PI, true);
    // ctx.fill();
    //
    // // para borrar
    //  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  }

  var latitude = 20.648737,
		longitude = -103.335283,
		map_zoom = 11;

  //google map custom marker icon - .png fallback for IE11
  var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
  var marker_url = ( is_internetExplorer11 ) ? 'img/cd-icon-location.png' : 'img/cd-icon-location.svg';

	//define the basic color of your map, plus a value for saturation and brightness
	var	main_color = '#2d313f',
		saturation_value= -20,
		brightness_value= 5;
  var style= [
		{
			//set saturation for the labels on the map
			elementType: "labels",
			stylers: [
				{saturation: saturation_value}
			]
		},
	    {	//poi stands for point of interest - don't show these lables on the map
			featureType: "poi",
			elementType: "labels",
			stylers: [
				{visibility: "off"}
			]
		},
		{
			//don't show highways lables on the map
	        featureType: 'road.highway',
	        elementType: 'labels',
	        stylers: [
	            {visibility: "off"}
	        ]
	    },
		{
			//don't show local road lables on the map
			featureType: "road.local",
			elementType: "labels.icon",
			stylers: [
				{visibility: "off"}
			]
		},
		{
			//don't show arterial road lables on the map
			featureType: "road.arterial",
			elementType: "labels.icon",
			stylers: [
				{visibility: "off"}
			]
		},
		{
			//don't show road lables on the map
			featureType: "road",
			elementType: "geometry.stroke",
			stylers: [
				{visibility: "off"}
			]
		},
		//style different elements on the map
		{
			featureType: "transit",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "poi",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "poi.government",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "poi.sport_complex",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "poi.attraction",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "poi.business",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "transit",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "transit.station",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "landscape",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]

		},
		{
			featureType: "road",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "road.highway",
			elementType: "geometry.fill",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		},
		{
			featureType: "water",
			elementType: "geometry",
			stylers: [
				{ hue: main_color },
				{ visibility: "on" },
				{ lightness: brightness_value },
				{ saturation: saturation_value }
			]
		}
	];
  var mapOptions = {
    center: new google.maps.LatLng(latitude, longitude),
      	zoom: map_zoom,
        draggable:false,
        maxZoom:11,
      	panControl: false,
      	zoomControl: false,
      	mapTypeControl: false,
      	streetViewControl: false,
      	mapTypeId: google.maps.MapTypeId.ROADMAP,
      	scrollwheel: false,
      	styles: style,
  };


 myMap = new google.maps.Map(document.getElementById("google-container"),mapOptions);

  function CustomZoomControl(controlDiv, myMap) {
    //grap the zoom elements from the DOM and insert them in the map
      var controlUIzoomIn= document.getElementById('cd-zoom-in'),
        controlUIzoomOut= document.getElementById('cd-zoom-out');
      controlDiv.appendChild(controlUIzoomIn);
      controlDiv.appendChild(controlUIzoomOut);

    // Setup the click event listeners and zoom-in or out according to the clicked element
    google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
        myMap.setZoom(myMap.getZoom()+1)
    });
    google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
        myMap.setZoom(myMap.getZoom()-1)
    });
   }

  var zoomControlDiv = document.createElement('div');
  var zoomControl = new CustomZoomControl(zoomControlDiv, myMap);

  //insert the zoom div on the top left of the map
  myMap.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);


  /*Cuando el boton continuar es precionado*/
  $('#btnContinuar').click(function() {
    if(clicked && avances[0] == 0)
    {
      incrementarAvance(1);
      /*Se agrega el otro index*/
      $('#seccion_1').hide();
      $('#seccion_2').show();
      $('footer').show();
      avances[0] = 1;
      // console.log('aca');
      //$('#mainContainer').append("<section id='bloque_2_container'></section>");
      //$("#bloque_2_container").load('../bloque_2.html');
      animacionCargando('mainRender');
      var loaderImg = new Image(),
          src = "images/iconos/config_bg.jpg";
      loaderImg.src = src;

      $(loaderImg).load(function() {
          ocultarAnimacion();
          $('#mainRender').show();
          $('#mainRender').attr('src', src);
      });


      $("#planta03").text( " " );



      $("#fraccSel").text( $("#infoTerreno .textoMenuInfo").text() );

      var p = ambiant.menu.aplicaOpcion(21);
      for( var i = 0; i < 4; i++ )
      {
        var img = json_ambiant.RetornaJson( p[i].id )['img'];
        // console.log( json_ambiant.RetornaJson( p[i].id )['img'] );
        agregarSlide( 2, p[i].id, "images/svg/" + img );
      }

      $('#' + p[0].id.toString() + ' .imgSliderPlantas' ).attr('src', 'images/svg/org_modelo_vangogh.svg');
      $("#" + p[0].id.toString() + " .imgSliderPlantas").css("height", "80px");
      $("#" + p[0].id.toString() + " .imgSliderPlantas").css("top", "-12px");
      $("#" + p[0].id.toString() + " .imgSliderPlantas").css('margin-top', '12px');
      $("#" + p[1].id.toString() + " .imgSliderPlantas").css('margin-top', '12px');
      $("#" + p[2].id.toString() + " .imgSliderPlantas").css('margin-top', '12px');
      ambiant.menu.aplicaOpcion(211);
      ambiant.menu.aplicaOpcion(221);
      cargarPlantas(211);

    }
  /*  else if (avances[2] == 1) {
      $('#seccion_1').hide();
      $('#seccion_2').show();
      $('#seccion_3').show();
      $('#seccion_4').hide();
      $("body").css("background", "url(" + "\'" + "images/iconos/config_bg.jpg\'" + ")");
      $("body").css("background-size", "100%");
    }*/
    else if(clicked && avances[0] == 1)
    {
      incrementarAvance(1);

      $('#seccion_1').hide();
      $('#seccion_2').show();
      $('footer').show();
      $('#mainRender').show();
      $("#mainRender").attr("src","images/iconos/config_bg.jpg");
      //$("body").css("background-size", "100%");

      $("#fraccSel").text( $("#infoTerreno .textoMenuInfo").text() );

    }
  });

  $('#slider button').mouseenter(function (){
  if(this.className == 'prev')
    $(this).find('img').attr("src",'img/cards_anterior_hover.svg');
  else
    $(this).find('img').attr("src",'img/cards_siguiente_hover.svg');
  });

  $('#slider button').mouseleave(function (){
  if(this.className == 'prev')
    $(this).find('img').attr("src",'img/cards_anterior.svg');
  else
    $(this).find('img').attr("src",'img/cards_siguiente.svg');
  });
});
