
function Slider() {

  var currentIndex = 0;
  var items;
  var itemAmt;
  var autoSlide;

  this.initialize = function (imagenes) {
    $('.container').append("<div style='display: inline-block;'><img src=' "+ imagenes[0] + "'/></div>");
    for (var i = 1; i < imagenes.length; i++) {
      $('.container').append("<div><img src=' "+ imagenes[i] + "'/></div>");
    }
    items =  $('.container div');
    itemAmt = items.length;

      autoSlide = setInterval(function() {
      currentIndex += 1;
      if (currentIndex > itemAmt - 1) {
        currentIndex = 0;
      }
      cycleItems();
    }, 2000);

    $('#slider').show();
  }

  this.remove = function()
  {
    $('.container').empty();
    clearInterval(autoSlide);
  }

  function cycleItems() {
    var item = $('.container div').eq(currentIndex);
    items.hide();
    item.css('display','inline-block');
  }


  $('.next').click(function() {
    // console.log(itemAmt);
    clearInterval(autoSlide);
    currentIndex += 1;
    if (currentIndex > itemAmt - 1) {
      currentIndex = 0;
    }
    cycleItems();
  });

  $('.prev').click(function() {
    clearInterval(autoSlide);
    currentIndex -= 1;
    if (currentIndex < 0) {
      currentIndex = itemAmt - 1;
    }
    cycleItems();
  });

}
