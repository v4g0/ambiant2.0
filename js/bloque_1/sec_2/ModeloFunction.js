var plantas_array = [1, 1, 0, 0, 0];
var modelos_array = [1, 0, 0];
var modelos_array_2 = [1, 0, 0];
var avances = [0, 0, 0];
var bloqueado = [0,0,0,0,0];
var contenedorAnimacion;

/* Funcion para agregar imagenes al slider */
function agregarSlide(seccion, id,imagen) {
  var slick = document.createElement('div');

  slick.id = id;
  slick.className = "slicks";
  slick.addEventListener("click", function(){

    if ( this.id[0] == '2' )
      clickSlickPlanta(this);
    ambiant.menu.aplicaOpcion(this.id);
    var mainRender = ambiant.renders.Reg_Rend();



    if ( /*this.id[0] == '2'*/1 )
    {
      var pos = 0;
      var n, n_2;
      if ( this.id[0] == '2' ){
        n = 211;
        n_2 =214;


      for ( ; n <= n_2; n++ )
      {
        if ( this.id != n.toString() )
        {
          img = 'images/svg/' + json_ambiant.RetornaJson( n.toString() )['img'];
          $('#'+n.toString() + ' .imgSliderPlantas').attr('src', img );
          $('#'+n.toString() + ' .imgSliderPlantas').css("height", "60px");
          $('#'+n.toString() + ' .imgSliderPlantas').css("top", "0px");
          modelos_array[pos] = 0;
        }
        else
        {
          img = 'images/svg/org_' + json_ambiant.RetornaJson( n.toString() )['img'];
          $('#'+n.toString() + ' .imgSliderPlantas').attr('src', img );
          modelos_array[pos] = 1;
        }
        pos = pos + 1;
      }
    }
    else if (this.id[0] == '3')
    {

        n = 311;
        n_2 = 313;

      for ( var n = 311; n <= 313; n++ )
      {
        if ( this.id != n.toString() )
        {
          img = 'images/iconos/nor_' + json_ambiant.RetornaJson( n.toString() )['img'];
          $('#'+n.toString() + ' .imgSliderPlantas').attr('src', img );

          // this.childNodes[1].style.top = '0px';
          $('#'+n.toString() + ' .imgSliderPlantas').css("height", "60px");
          $('#'+n.toString() + ' .imgSliderPlantas').css("top", "0px");
          modelos_array_2[pos] = 0;
        }
        else
        {
          img = 'images/iconos/org_' + json_ambiant.RetornaJson( n.toString() )['img'];
          $('#'+n.toString() + ' .imgSliderPlantas').attr('src', img );
          // this.childNodes[1].style.top = '-10px';
          // $('#'+n.toString() + ' .imgSliderPlantas').css("height", "80px");
          // $('#'+n.toString() + ' .imgSliderPlantas').css("top", "-10px");
          modelos_array_2[pos] = 1;
        }
        pos = pos + 1;
      }
      $("#mainRender").attr("src", "images/fachadas/" + mainRender['Fachada'] ); //FA-VG-V2-P1-CF1.jpg
    }


  }

  });

  $(slick).on("mouseenter", function(){

    var im =  $('#'+this.id.toString() + ' .imgSliderPlantas').attr('src') ;
    if ( im.indexOf('org') > -1 )
    {
      this.childNodes[1].style.display = 'inline';
      this.childNodes[1].style.top = '-12px';
      // jQuery(this.childNodes[1]).animate({
      //   top: '-12px'
      // });
      // $("#" + this.id.toString() + " .imgSliderPlantas").css("height", "80px");
      // $("#" + this.id.toString() + " .imgSliderPlantas").css("top", "-12px");

      $("#" + this.id.toString() + " .imgSliderPlantas").animate({
        height: '80px',
        top:    '-12px'
      });

    }
    else {

      if ( this.id == '312')
      {
        this.childNodes[1].style.left = '-25px';
        // jQuery(this.childNodes[1]).animate({
        //   left: '-25px'
        // });
      }
      else if ( this.id == '313'){
        this.childNodes[1].style.left = '-5px';
        // jQuery(this.childNodes[1]).animate({
        //   left: '-5px'
        // });
      }

      img = 'images/svg/org_' + json_ambiant.RetornaJson( this.id )['img'];
      $('#'+this.id.toString() + ' .imgSliderPlantas').attr('src', img );
      this.childNodes[1].style.display = 'inline';


      // $("#" + this.id.toString() + " .imgSliderPlantas").css("height", "80px");
      // $("#" + this.id.toString() + " .imgSliderPlantas").css("top", "-12px");
      $("#" + this.id.toString() + " .imgSliderPlantas").animate({
        height: '80px',
        top:    '-12px'
      });
      jQuery(this.childNodes[1]).animate({
        top: '-12px'
      });
      // this.childNodes[1].style.top = '-12px';
    }

    // jQuery(this).unbind("mouseover");


  });
  $(slick).on("mouseleave", function(){
    if ( this.id[0] == '3')
    {
      var im =  $('#'+this.id.toString() + ' .imgSliderPlantas').attr('src') ;
      if ( modelos_array_2[ parseInt(this.id[2]) - 1 ] == 1 )
      {
        this.childNodes[1].style.display = 'none';
        // $('#'+this.id.toString() + ' .imgSliderPlantas').attr('src', img );


        jQuery(this.childNodes[1]).animate({
          top: '0px'
        });
        // $("#" + this.id.toString() + " .imgSliderPlantas").css("height", "80px");
        // $("#" + this.id.toString() + " .imgSliderPlantas").css("top", "-12px");
        $("#" + this.id.toString() + " .imgSliderPlantas").animate({
          height: '80px',
          top:    '-12px'
        })
      }
      else {
        this.childNodes[1].style.display = 'none';
        img = 'images/svg/' + json_ambiant.RetornaJson( id )['img'];
        $('#'+this.id.toString() + ' .imgSliderPlantas').attr('src', img );

        // this.childNodes[1].style.top = '0px';
        jQuery(this.childNodes[1]).animate({
          top: '0px'
        });
        // $("#" + this.id.toString() + " .imgSliderPlantas").css("height", "60px");
        // $("#" + this.id.toString() + " .imgSliderPlantas").css("top", "0px");
        $("#" + this.id.toString() + " .imgSliderPlantas").animate({
          height: '60px',
          top:    '0px'
        });
      }
    }
    if ( this.id[0] == '2')
    {

      var im =  $('#'+this.id.toString() + ' .imgSliderPlantas').attr('src') ;
      if ( modelos_array[ parseInt(this.id[2]) - 1 ] == 1 )
      {
        this.childNodes[1].style.display = 'none';
        // $('#'+this.id.toString() + ' .imgSliderPlantas').attr('src', img );

        // this.childNodes[1].style.top = '-12px';
        jQuery(this.childNodes[1]).animate({
          top: '-12px'
        });
        // $("#" + this.id.toString() + " .imgSliderPlantas").css("height", "80px");
        // $("#" + this.id.toString() + " .imgSliderPlantas").css("top", "-12px");
        $("#" + this.id.toString() + " .imgSliderPlantas").animate({
          height: '80px',
          top:    '-12px'
        });
      }
      else {

        this.childNodes[1].style.display = 'none';
        img = 'images/svg/' + json_ambiant.RetornaJson( id )['img'];
        $('#'+this.id.toString() + ' .imgSliderPlantas').attr('src', img );

        // this.childNodes[1].style.top = '0px';
        jQuery(this.childNodes[1]).animate({
          top: '0px'
        });
        // $("#" + this.id.toString() + " .imgSliderPlantas").css("height", "60px");
        // $("#" + this.id.toString() + " .imgSliderPlantas").css("top", "0px");
        $("#" + this.id.toString() + " .imgSliderPlantas").animate({
          height: '60px',
          top:    '0px' });
      }
    }
    // jQuery(this).bind('mouseover');


  });
  // $(slick).unbind('mouseout');
  var img = document.createElement('img');
  img.className = "imgSliderPlantas";

  img.src = imagen;
  img.setAttribute('style', 'height: 60px; top: 3px; margin-top: 8px');



  slick.appendChild(img);

  var texto = document.createElement('p');
  texto.className = 'textoModelo';
  texto.style.display = 'none';
  texto.innerHTML = json_ambiant.RetornaJson( id )['nom'];
  slick.appendChild(texto);

  if (seccion == 2 )
  {

    $('#sliderSec2').slick('slickAdd',slick);
  }
  else if (seccion == 3) {
    $('#sliderSec3').slick('slickAdd',slick);
  }
}

$(document).ready(function(){
  $('#vision_360').click(function(){
    window.open("http://www.grupoambiant.com/botero360/");
  });
  $('#btnVista360').click(function(){
    window.open("http://www.grupoambiant.com/botero360/");
  });

  $('#sliderSec2').slick({
    dots: false,
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 3
  });

  $('#btnContinuarPlantas').click(function() {
    if( avances[1] == 0)
    {
      resizeMainRender(screen.width,screen.height);
      incrementarAvance(2);
      $('#seccion_2').hide();
      $('#bloque_2').show();
      $('#seccion_3').show();
      $("body").css("background", "none");

      animacionCargando('mainContainer');

      var p = ambiant.menu.aplicaOpcion(31);

      for( var i = 0; i < p.length; i++)
      {
        var img = json_ambiant.RetornaJson( p[i].id )['img'];

        agregarSlide( 3, p[i].id, "images/iconos/" + "nor_" + img );
      }

      $("#" + p[0].id.toString() + " .imgSliderPlantas").attr("src", "images/iconos/org_estilo_moderno.svg");
      $("#" + p[0].id.toString() + " .imgSliderPlantas").css("height", "80px");
      $("#" + p[0].id.toString() + " .imgSliderPlantas").css("top", "-12px");
      $("#" + p[0].id.toString() + " .imgSliderPlantas").css('margin-top', '8px');
      p = ambiant.menu.aplicaOpcion(3);

      for( var i = 1; i < p.length; i++)
      {
        var j = json_ambiant.RetornaJson( p[i].id )
        var img = j['img'];
        var sub = ambiant.menu.aplicaOpcion(p[i].id);

        var lista_img = [];
        var lista_nom = [];
        var lista_ids = [];
        var lista_cos = [];
        var visible = 1;
        for( var s = 0; s < sub.length; s++)
        {

          var data = json_ambiant.RetornaJson( sub[s].id );

          lista_img.push( "images/jpg/" + data['img'] );
          lista_nom.push( data['nom'] );
          lista_ids.push( sub[s].id );

          if ( data['plus'] === 'true' )
            lista_cos.push(1);
          else lista_cos.push(0);

          if ( data['rend'] == 'null' ) visible = 0;
        }

        var item;
        var block = new bloque();
        item = block.crearBloque(0, j['nom'],visible,lista_cos, lista_img, lista_nom, lista_ids);

        item.setAttribute( "style", "background: url(" + "\'" + "images/iconos/" + img +  " \'); background-repeat: no-repeat;background-size:100%");
        item.id = p[i].id;

        $('.leftMenu').append(item);

        if(i == p.length-1 && !leftContainerPG)
        {
          item.childNodes[0].className = "contInfoSlider contInfoSliderPC contInfoSliderDoblePCUltimo";
        }
      }

      ambiant.menu.aplicaOpcion(311);
      var mainRender = ambiant.renders.Reg_Rend()['Fachada'] ;

      /*animacion para cuando carga*/
      var loaderImg = new Image(),
          src = "images/fachadas/" + mainRender;

      loaderImg.src = src;

      $(loaderImg).load(function() {
          ocultarAnimacion();
          $('#mainRender').attr('src', src);
      });


      avances[1] = 1;


    }
else if (avances[1] == 1) {
  console.log('regresa seccion 3');
  resizeMainRender(screen.width,screen.height);
  incrementarAvance(2);
  $('#seccion_2').hide();
  $('#bloque_2').show();
  $('#seccion_3').show();
  $('#seccion_4').hide();
  $("body").css("background", "none");

  animacionCargando('mainContainer');


  avances[1] = 1;


  ambiant.menu.aplicaOpcion(311);
  var mainRender = ambiant.renders.Reg_Rend()['Fachada'] ;

  var loaderImg = new Image(),
      src = "images/fachadas/" + mainRender;

  loaderImg.src = src;

  $(loaderImg).load(function() {
      ocultarAnimacion();
      $('#mainRender').attr('src', src);
  });


  $('#btnContinuarBloque_2 .btn_modelo .textoMenuBoton').text("Continuar");
  $('#btnContinuarBloque_2 .btn_modelo #simbolo').text("›");

  }
  });

  //Se pone por defecto el la primer seccion selecionada en el menu logo




});


/* cuando se preciona algun slick en el slider*/
function clickSlickPlanta(slick) {
  // console.log(slick.id);
  ambiant.menu.aplicaOpcion(slick.id);
  cargarPlantas(slick.id);

}
function cargarPlantas(id)
{

  var info = json_ambiant.RetornaJson( id );
  var fondo = info['ico'];
  var icono = info['img'];
  var nombre = info['nom'];


  $(".imgContainerMenuModelo").css("background", "url(\'" + "images/jpg/" + fondo + "\')");
  $(".imgContainerMenuModelo").css("background-size", "100% 100%");
  document.getElementById('nombreModelo').innerHTML= nombre;

  $(".imgContainerMenuModelo #iconoModelo").attr("data", "images/svg/nor_" + icono );

  if(nombre == 'Botero')
    $('#vision_360').show();
  else
    $('#vision_360').hide();
  // $(".imgContainerMenuModelo #iconoModelo").attr("src", "/images/svg/" + "dali.svg" );

/* No funciona al cargar en tiempo real
    /////// Modificar SVG
    var a = document.getElementById("iconoModelo");
    console.log(a);
    var svg = a.contentDocument;
    console.log(svg);
    var svgItem = svg.getElementsByTagName("path");

    for( var i = 0; i < svgItem.length; i++ )
    {
      console.log( svgItem[i].setAttribute("fill", "#FFFFFF") );
    }

    ////////

*/
  renders = ambiant.renders.Reg_Rend();
  var planta = renders['PBaja'];
  planta = "images/plantas/" +  planta;
  //planta = "url(" + "\'" + planta +  " \')";

  $('#plantaBaja').find('img').eq(0).attr("src",  planta);


  planta = renders['PAlta2'];
  planta = "images/plantas/" +  planta;


  $('#segPiso_1').css("background",  planta);
  $('#segPiso_1').find('img').eq(0).attr("src",  planta);


  planta = renders['PAlta3'];
  planta = "images/plantas/" +  planta;


  $('#segPiso_2').css("background",   planta);
  $('#segPiso_2').find('img').eq(0).attr("src",  planta);


  planta = renders['PRG'];
  planta = "images/plantas/" +  planta;
  //planta = "url(" + "\'" + planta +  " \')";

  $('#terPiso_1').css("background",   planta);
  $('#terPiso_1').find('img').eq(0).attr("src",  planta);

  planta = renders['PFR'];
  planta = "images/plantas/" +  planta;
  //planta = "url(" + "\'" + planta +  " \')";

  $('#terPiso_2').css("background",  planta);
  $('#terPiso_2').find('img').eq(0).attr("src",  planta);

  $("#construccion").text( renders["Metros"] + " m2");

}

function changeOp(boton) {
  $(boton).parent().find('img').eq(0).css('opacity',0.25);
}

function returnOp(boton) {
  $(boton).parent().find('img').eq(0).css('opacity',1);
}

function marcarPlanta(boton) {
  //Padre es el contenedor principal de el boton
  var btnContinuarSegPiso_1 = $("#btnContinuarSegPiso_1 ");
  var btnContinuarSegPiso_2 = $("#btnContinuarSegPiso_2 ");
  var btnContinuarTerPiso_1 = $("#btnContinuarTerPiso_1 ");
  var btnContinuarTerPiso_2 = $("#btnContinuarTerPiso_2 ");

  var padre = $(boton).parent();
  var className = $(padre).attr('class');
  /*Cuando ya esta selecionado*/
  if ( padre.attr("id") != "btnContinuarPB")
  {

    if(className == 'btn_container_plantas_2 btn_plantas_sel' || className == 'btn_container_plantas_3 btn_plantas_sel')
    {

      greenColor(padre);
      $(padre).removeClass("btn_plantas_sel");
      $(padre).addClass("btn_plantas_des");
      $(padre).find('div').eq(0).removeClass("infoPlanta_sel");
      $(padre).find('div').eq(0).addClass("infoPlanta_des");
      changeOp(padre);

      ambiant.menu.aplicaOpcion(221);

      renders = ambiant.renders.Reg_Rend();
      $("#construccion").text( renders["Metros"] + " m2");
    }
    /*Cuando ya esta selecionado*/
    else
    {
      redColor(padre);
      $(padre).removeClass("btn_plantas_des");
      $(padre).addClass("btn_plantas_sel");
      $(padre).find('div').eq(0).removeClass("infoPlanta_des");
      $(padre).find('div').eq(0).addClass("infoPlanta_sel");
      returnOp(padre);
    }



    var plantaID;
    if ( $(padre).attr('id') === "btnContinuarSegPiso_1"  )
    {

      plantaID = plantas_array[1];

      if ( plantaID == 0 )
      {
        plantas_array[1] = !plantaID;
        for( var i = 2; i < 5; i++ )
        {
          // eliminar de info
          plantas_array[i] = 0;
        }



        $(btnContinuarSegPiso_2).removeClass("btn_plantas_sel");
        $(btnContinuarSegPiso_2).addClass("btn_plantas_des");
        $(btnContinuarSegPiso_2).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarSegPiso_2).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarSegPiso_2);
        changeOp(btnContinuarSegPiso_2);


        $(btnContinuarTerPiso_1).removeClass("btn_plantas_sel");
        $(btnContinuarTerPiso_1).addClass("btn_plantas_des");
        $(btnContinuarTerPiso_1).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarTerPiso_1).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarTerPiso_1);
        changeOp(btnContinuarTerPiso_1);

        $(btnContinuarTerPiso_2).removeClass("btn_plantas_sel");
        $(btnContinuarTerPiso_2).addClass("btn_plantas_des");
        $(btnContinuarTerPiso_2).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarTerPiso_2).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarTerPiso_2);
        changeOp(btnContinuarTerPiso_2);

        $("#planta02").text( "Segundo Piso, 2 Recámaras" );
        $("#planta03").text( " " );

        ambiant.menu.aplicaOpcion(221);

        renders = ambiant.renders.Reg_Rend();
        $("#construccion").text( renders["Metros"] + " m2");

      }
      else
      {
        // console.log($(padre).attr('id'));
        plantas_array[1] = 0;
        plantas_array[2] = 1;
        $(btnContinuarSegPiso_2).removeClass("btn_plantas_des");
        $(btnContinuarSegPiso_2).addClass("btn_plantas_sel");
        $(btnContinuarSegPiso_2).find('div').eq(0).removeClass("infoPlanta_des");
        $(btnContinuarSegPiso_2).find('div').eq(0).addClass("infoPlanta_sel");
        greenIColor(btnContinuarSegPiso_2);
        returnOp(btnContinuarSegPiso_2);

        $("#planta02").text( "Segundo Piso, 3 Recámaras" );
        $("#planta03").text( " " );
        ambiant.menu.aplicaOpcion(221);

        renders = ambiant.renders.Reg_Rend();
        $("#construccion").text( renders["Metros"] + " m2");
      }


    }
    else if ( $(padre).attr('id') === "btnContinuarSegPiso_2" )
    {
      console.log($(padre).attr('id'));
      plantaID = plantas_array[2];
      if ( plantas_array[2] == 0 )
      {
        plantas_array[2] = !plantaID;
        plantas_array[1] = 0;

        $(btnContinuarSegPiso_1).removeClass("btn_plantas_sel");
        $(btnContinuarSegPiso_1).addClass("btn_plantas_des");
        $(btnContinuarSegPiso_1).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarSegPiso_1).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarSegPiso_1);
        changeOp(btnContinuarSegPiso_1);

        $("#planta02").text( "Segundo Piso, 3 Recámaras" );
        ambiant.menu.aplicaOpcion(222);

        renders = ambiant.renders.Reg_Rend();
        $("#construccion").text( renders["Metros"] + " m2");

      }
      else {
        plantas_array[2] = !plantaID;
        plantas_array[1] = 1;
        plantas_array[3] = 0;

        $(btnContinuarSegPiso_1).removeClass("btn_plantas_des");
        $(btnContinuarSegPiso_1).addClass("btn_plantas_sel");
        $(btnContinuarSegPiso_1).find('div').eq(0).removeClass("infoPlanta_des");
        $(btnContinuarSegPiso_1).find('div').eq(0).addClass("infoPlanta_sel");
        greenIColor(btnContinuarSegPiso_1);
        returnOp(btnContinuarSegPiso_1);

        $(btnContinuarTerPiso_1).removeClass("btn_plantas_sel");
        $(btnContinuarTerPiso_1).addClass("btn_plantas_des");
        $(btnContinuarTerPiso_1).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarTerPiso_1).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarTerPiso_1);
        changeOp(btnContinuarTerPiso_1);

        $(btnContinuarTerPiso_2).removeClass("btn_plantas_sel");
        $(btnContinuarTerPiso_2).addClass("btn_plantas_des");
        $(btnContinuarTerPiso_2).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarTerPiso_2).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarTerPiso_2);
        changeOp(btnContinuarTerPiso_2);


        $("#planta02").text( "Segundo Piso, 2 Recámaras" );
        $("#planta03").text( " " );
        ambiant.menu.aplicaOpcion(221);

        renders = ambiant.renders.Reg_Rend();
        $("#construccion").text( renders["Metros"] + " m2");
      }
    }
    else if ( $(padre).attr('id') === "btnContinuarTerPiso_1" )
    {
      // console.log($(padre).attr('id'));

      plantaID = plantas_array[3];
      if ( plantaID == 0 )
      {
        plantas_array[3] = !plantaID;
        plantas_array[1] = 0;
        plantas_array[2] = 1;
        plantas_array[4] = 0;

        $(btnContinuarTerPiso_2).removeClass("btn_plantas_sel");
        $(btnContinuarTerPiso_2).addClass("btn_plantas_des");
        $(btnContinuarTerPiso_2).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarTerPiso_2).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarTerPiso_2);
        changeOp(btnContinuarTerPiso_2);

        $(btnContinuarSegPiso_2).removeClass("btn_plantas_des");
        $(btnContinuarSegPiso_2).addClass("btn_plantas_sel");
        $(btnContinuarSegPiso_2).find('div').eq(0).removeClass("infoPlanta_des");
        $(btnContinuarSegPiso_2).find('div').eq(0).addClass("infoPlanta_sel");
        greenIColor(btnContinuarSegPiso_2);
        returnOp(btnContinuarSegPiso_2);

        $(btnContinuarSegPiso_1).removeClass("btn_plantas_sel");
        $(btnContinuarSegPiso_1).addClass("btn_plantas_des");
        $(btnContinuarSegPiso_1).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarSegPiso_1).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarSegPiso_1);
        changeOp(btnContinuarSegPiso_1);

        $("#planta03").text( "Tercer Piso, Roof Garden" );
        $("#planta02").text( "Segundo Piso, 3 Recámaras" );

        ambiant.menu.aplicaOpcion(223);

        renders = ambiant.renders.Reg_Rend();
        $("#construccion").text( renders["Metros"] + " m2");

      }
      else {
        plantas_array[3] = !plantaID;
        $("#planta03").text( " " );
        ambiant.menu.aplicaOpcion(222);

        renders = ambiant.renders.Reg_Rend();
        $("#construccion").text( renders["Metros"] + " m2");
      }
    }
    else if ( $(padre).attr('id') === "btnContinuarTerPiso_2" )
    {
      // console.log($(padre).attr('id'));
      plantaID = plantas_array[4];
      if ( plantaID == 0 )
      {
        plantas_array[4] = !plantaID;
        plantas_array[1] = 0;
        plantas_array[2] = 1;
        plantas_array[3] = 0;

        $(btnContinuarTerPiso_1).removeClass("btn_plantas_sel");
        $(btnContinuarTerPiso_1).addClass("btn_plantas_des");
        $(btnContinuarTerPiso_1).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarTerPiso_1).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarTerPiso_1);
        changeOp(btnContinuarTerPiso_1);

        $(btnContinuarSegPiso_2).removeClass("btn_plantas_des");
        $(btnContinuarSegPiso_2).addClass("btn_plantas_sel");
        $(btnContinuarSegPiso_2).find('div').eq(0).removeClass("infoPlanta_des");
        $(btnContinuarSegPiso_2).find('div').eq(0).addClass("infoPlanta_sel");
        greenIColor(btnContinuarSegPiso_2);
        returnOp(btnContinuarSegPiso_2);

        $(btnContinuarSegPiso_1).removeClass("btn_plantas_sel");
        $(btnContinuarSegPiso_1).addClass("btn_plantas_des");
        $(btnContinuarSegPiso_1).find('div').eq(0).removeClass("infoPlanta_sel");
        $(btnContinuarSegPiso_1).find('div').eq(0).addClass("infoPlanta_des");
        greyColor(btnContinuarSegPiso_1);
        changeOp(btnContinuarSegPiso_1);

        $("#planta03").text( "Tercer Piso, Family Room" );
        $("#planta02").text( "Segundo Piso, 3 Recámaras" );

        ambiant.menu.aplicaOpcion(224);

        renders = ambiant.renders.Reg_Rend();
        $("#construccion").text( renders["Metros"] + " m2");

      }
      else {
        plantas_array[4] = !plantaID;
        $("#planta03").text( " " );
        ambiant.menu.aplicaOpcion(222);

        renders = ambiant.renders.Reg_Rend();
        $("#construccion").text( renders["Metros"] + " m2");
      }
    }

    cerrarAnim(padre,null);
  }



}

function abrirAnim(boton,index) {
  /*
  */
  if(!bloqueado[index])
  {
    // console.log($(boton).attr('class'));
    if($(boton).attr('class')=="btn_container_plantas_m btn_plantas_sel_m" || $(boton).attr('class') ==  "btn_container_plantas_2 btn_plantas_des"|| $(boton).attr('class') ==  "btn_container_plantas_3 btn_plantas_des")
      greenColor(boton);
    else
      redColor(boton);
    $(boton).animate({
        height: "170px",
        marginTop: "162px"
      }, 300 );



    $(boton).find('div').eq(0).animate({
        height: "120px",

      }, 300 );

      $(boton).find('button').eq(1).animate({
          height: "50px",

        }, 300 );
        bloqueado[index]=false;
  }


}

function cerrarAnim(boton,index) {

  // console.log($(boton).attr('class'));
  if(index!=null)
  {
    if($(boton).attr('class') ==  "btn_container_plantas_2 btn_plantas_des" || $(boton).attr('class') ==  "btn_container_plantas_3 btn_plantas_des")
      greyColor(boton);

    if($(boton).attr('class') ==  "btn_container_plantas_2 btn_plantas_sel"|| $(boton).attr('class') ==  "btn_container_plantas_3 btn_plantas_sel" || $(boton).attr('class')=="btn_container_plantas_m btn_plantas_sel_m")
      greenIColor(boton);
  }



  bloqueado[index] =true;
  $(boton).animate({
    height: "34px",
    marginTop:"298px",
    },
    {
      duration:10,
      complete : function() {
    }
  });


  $(boton).find('div').eq(0).animate({
      height: "0px",
    },
    {
      duration:10,
      complete : function() {
    }
  });

  $(boton).find('button').eq(1).animate({
      height: "34px",
    },
    {
      duration:10,
      complete : function() {
      bloqueado[index] =false;
    }
  });

}


//Cuando se hace el mouseenter

//Cuando se hace el mouseleave

function verDetalle(id,flag)
{
  $('.verDetalleContainer').show();
  $('.imgZoomContainer').css('height',$('.imgZoomContainer').height());
  $('.imgZoomContainer').css('width',$('.imgZoomContainer').width());
  $('.imgZoomContainer').css('margin-left',$('.imgZoomContainer').width()/2 * -1);
  $('.imgZoomContainer').css('margin-top',$('.imgZoomContainer').height()/2 * -1);

  $('.background').css('height',$('.background').height());
  $('.background').css('width',$('.background').width());
  $('.background').css('margin-left',$('.background').width()/2 * -1);
  $('.background').css('margin-top',$('.background').height()/2 * -1);


  var renders = ambiant.renders.Reg_Rend();

  if (id == 221 && flag == 0)
    planta = renders['PBaja'];
  else if (id == 221 && flag == 1 )
    planta = renders['PAlta2'];
  else if (id == 222 && flag == 0)
    planta = renders['PAlta3'];
  else if (id == 223 && flag == 0)
    planta = renders['PRG'];
  else if (id == 224 && flag == 0 )
    planta = renders['PFR'];


  inicializarZoomView(planta);
}

function changeColor(boton) {
  if($(boton).parent().attr('class') == "infoPlanta infoPlanta_des" || $(boton).parent().attr('class') == "infoPlanta infoPlanta_des_m")
  {
    boton.style.color = '#52B03F';
    boton.childNodes[1].src = "img/cards_zoom_des.svg";
  }

  else
  {
    boton.style.color = '#E64949';
    boton.childNodes[1].src = "img/cards_zoom_sel.svg";
  }

}

function returnColor(boton) {
    boton.style.color = '#fff';
    boton.childNodes[1].src = "img/cards_zoom.svg";
}

function greenColor(boton) {
  $(boton).css('border-bottom-width','1px');
  $(boton).css('background-color','#52B03F');
  $(boton).css('border-bottom-color','#71D656');
  $(boton).css('box-shadow','0px 4px 0px #3C9930');
  $(boton).find('div').css("border-bottom-color","#3c9930");
  $(boton).find('button').eq(1).attr('data-content','Agregar ✓');
  $(boton).find('button').eq(1).css('text-shadow','0px 1px #3C9930');
}

function greenIColor(boton) {
  $(boton).css('border-bottom-width','1px');
  $(boton).css('background-color','#65C94A');
  $(boton).css('border-bottom-color','#78E05C');
  $(boton).css('box-shadow','0px 4px 0px #4DA635');
  $(boton).find('div').css("border-bottom-color","#65C94A");
  $(boton).find('button').eq(1).attr('data-content','Agregada ✓');
  $(boton).find('button').eq(1).css('text-shadow','0px 1px #4DA635');
}

function redColor(boton) {
  $(boton).css('border-bottom-width','1px');
  $(boton).css('border-bottom-color','#FA6464');
  $(boton).css('background-color','#E64949');
  $(boton).css('box-shadow','0px 4px 0px #B53939');
  $(boton).find('div').css("border-bottom-color","#b53939");
  $(boton).find('button').eq(1).attr('data-content','Quitar x');
  $(boton).find('button').eq(1).css('text-shadow','0px 1px #B53939');
}
function greyColor(boton) {
  $(boton).css('border-bottom-width','1px');
  $(boton).css('border-bottom-color','#E8E8E8');
  $(boton).css('background-color','#D6D4D2');
  $(boton).css('box-shadow','0px 4px 0px #B5B2AC');
  $(boton).find('div').css("border-bottom-color","#D6D4D2");
  $(boton).find('button').eq(1).attr('data-content','Agregar');
  $(boton).find('button').eq(1).css('text-shadow','0px 1px #B5B2AC');
}
