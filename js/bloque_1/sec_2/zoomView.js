$(document).ready(function() {
  var myInterval = false;
  $('.up_zoom').mouseover(function(){
     myInterval = setInterval(function(){
         mouseUp();
     }, 10);
  });

  $('.up_zoom').mouseout(function(){
     clearInterval(myInterval);
     myInterval = false;
     document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/ambas.png), auto';
  });

  $('.down_zoom').mouseover(function(){
     myInterval = setInterval(function(){
         mouseDown();
     }, 10);
  });

  $('.down_zoom').mouseout(function(){
     clearInterval(myInterval);
     myInterval = false;
     document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/ambas.png), auto';

  });

  $('#imgZoomContainer').bind('DOMMouseScroll', function(e){
    clearTimeout($.data(this, 'timer'));
    $.data(this, 'timer', setTimeout(function() {
      document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/ambas.png), auto';
    }, 200));

    if(e.originalEvent.detail > 0) {
        //scroll down
        mouseDown();
        mouseDown();
        mouseDown();
        mouseDown();
    }else {
        //scroll up
        mouseUp();
        mouseUp();
        mouseUp();
        mouseUp();
    }

    //prevent page fom scrolling
    return false;
  });

  //IE, Opera, Safari
  $('#imgZoomContainer').bind('mousewheel', function(e){

     clearTimeout($.data(this, 'timer'));
     $.data(this, 'timer', setTimeout(function() {
        document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/ambas.png), auto';
     }, 200));

      if(e.originalEvent.wheelDelta < 0) {
          document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/abajo.png), auto';
          mouseDown();
          mouseDown();
          mouseDown();
          mouseDown();
      }else {
          //scroll up
          document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/arriba.png), auto';
          mouseUp();
          mouseUp();
          mouseUp();
          mouseUp();
      }

      //prevent page fom scrolling
      return false;
  });

  $(document).keyup(function(e) {
     if (e.keyCode == 27) {
       cerrarZoom();
    }
});

});

var heightPadre;
var topRiel;
var topRef;
function cerrarZoom()
{
  $('.verDetalleContainer').hide();
}

function inicializarZoomView(imagen) {
  document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/ambas.png), auto';
  animacionCargando('imgZoomContainer');
  heightPadre = $('.imgZoomContainer').height();

  var dir = 'images/plantas/' + imagen;

  $('#imgRef').attr("src",dir);

  var loaderImg = new Image(),
      src = 'images/plantas_hd/' + imagen;

  loaderImg.src = src;

  $(loaderImg).load(function() {
      ocultarAnimacion();
      $('.imgZoomContainer').css("background", "url('" + src + "')");
      $('.imgZoomContainer').css("background-size", "100% 200%");
      $('.imgZoomContainer').css("background-position","0% 50%");
  });



  topRiel = 50;
  topRef= 0;
  topRef = topRef + 83.33;
  $('.refBox').css('top', topRef + 'px');

}

function mouseUp()
{
  console.log('si funciono');
  document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/arriba.png), auto';
  var newTop;

  if(topRiel != 100)
  {
    newTop = topRiel + 1 + "%";
    topRiel++;
    $('.imgZoomContainer').css("background-position","0%" + newTop );


    topRef = topRef + 1.66;
    $('.refBox').css('top', topRef + 'px');
  }
}

function mouseDown() {

  document.getElementById('imgZoomContainer').style.cursor = 'url(../configurador/img/abajo.png), auto';
  var newTop;

  if(topRiel != 0)
  {
    newTop = topRiel - 1 + "%";
    topRiel--;
    $('.imgZoomContainer').css("background-position","0%" + newTop );

    topRef = topRef - 1.66;
    $('.refBox').css('top', topRef + 'px');
  }
}
