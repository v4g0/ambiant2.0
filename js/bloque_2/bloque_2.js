var click = null;
var finalizar = false;
var clickLeftContainer = null;
var final_ready = 0;
var array_eliminados = new Array();

function resizeMainRender(ancho,alto) {
  var nuevoAncho = alto * 1570/883;
  var left = (nuevoAncho - ancho)/2;
  var top = (alto - $(window).height())/2;
  nuevoAncho =  Math.round(nuevoAncho);
  left =  Math.round(-left);
  top =  Math.round(-top);
  $('#mainRender').width(nuevoAncho);
  $('#mainRender').height(alto);
  $('#mainRender').css('left', left);
  $('#mainRender').css('bottom', top);
}

function originalMainRender(ancho) {

  $('#mainRender').width(ancho);
  $('#mainRender').height($(window).height());
  $('#mainRender').css('left', 0);
  $('#mainRender').css('bottom', 0);
}

function showHideLeftContainerExtended(leftContainer)
{
    animacionCargando('mainContainer');


    ambiant.menu.aplicaOpcion(parseInt(leftContainer.id));
    var mainRender;
    if ( leftContainer.id[0] == '3' )
      mainRender = ambiant.renders.Reg_Rend()['Fachada'] ;
    else if (leftContainer.id[0] == '4')
      mainRender = ambiant.renders.Reg_Rend()['Sala'] ;
    else if (leftContainer.id[0] == '5')
      mainRender = ambiant.renders.Reg_Rend()['Cocina'] ;
    else if (leftContainer.id[0] == '6')
      mainRender = ambiant.renders.Reg_Rend()['Bano'] ;
    else if (leftContainer.id[0] == '7')
      mainRender = ambiant.renders.Reg_Rend()['Closet'] ;

    var bandera=0;
    if(click==null)
    {
      // console.log( 'null');
      click=leftContainer;
      bandera=1;
      $(leftContainer).find('div').eq(0).show();
    }

    if(click!=leftContainer)
    {
      // console.log( 'Diferente');
      $(click).attr("style", "background: url(" + "\'" + "images/iconos/" + json_ambiant.RetornaJson($(click).attr("id"))['img'] +  " \'); background-repeat: no-repeat;background-size:100%;");
      $(click).find('div').eq(0).hide();
      click=leftContainer;
      $(click).find('div').eq(0).show();


      var loaderImg = new Image(),
          src = "images/fachadas/" + mainRender;

      loaderImg.src = src;

      $(loaderImg).load(function() {
          ocultarAnimacion();
          $('#mainRender').attr('src', src);
      });
    }
    else
    {
      // console.log( 'igual');
      var loaderImg = new Image(),
          src = "images/fachadas/" + mainRender;

      loaderImg.src = src;

      $(loaderImg).load(function() {
          ocultarAnimacion();
          $('#mainRender').attr('src', src);
      });0
      if(!bandera)
      {
        $(click).find('div').eq(0).hide();
        click=null;
      }
      bandera=0;
    }
}

function orangeEfectIn(leftContainer)
{
  $(leftContainer).attr("style", "background: url(" + "\'" + "images/iconos/org_" + json_ambiant.RetornaJson(leftContainer.id)['img'] +  " \'); background-repeat: no-repeat;background-size:100%;");
}
function orangeEfectOut(leftContainer)
{
  if(leftContainer != click || click == null)
    $(leftContainer).attr("style", "background: url(" + "\'" + "images/iconos/" + json_ambiant.RetornaJson($(leftContainer).attr("id"))['img'] +  " \'); background-repeat: no-repeat;background-size:100%;");
}

function showContainerInfo(leftContainer) {
  $(leftContainer).find('div.contInfoSlider').show();


  // Buscamos por los hijos que contengan el maquetado a eliminar
  var candidato;
  if(leftContainerPG)
    candidato= $(leftContainer).find('div.contInfoSlider').find('div.itemInfoContainerPG');
  else
    candidato= $(leftContainer).find('div.contInfoSlider').find('div.itemInfoContainerPC');
  // console.log(candidato);
  // sacamos el padre de uno de ellos y sacamos el id para buscar si hay candado
  var padre = parseInt(candidato.eq(0).attr('id')[0])*10 +
              parseInt(candidato.eq(0).attr('id')[1]);
  // console.log(padre);
  // buscamos el id para obtener candados
  var res = ambiant.menu.aplicaOpcion(padre);
  // bandare para conocer si se elimino en el primer elemento
  var flag = false;
  // console.log( array_eliminados );

  // iteramos por cada elemento hasta que este contanga un * y le eliminamos
  for( var i = 0; i < candidato.length; i++ )
  {

    if ( candidato.eq(i).attr('id') == res[i].id )
    {

      if ( res[i].texto.indexOf('*') > -1  )
      {
        if ( !array_eliminados[candidato.eq(i).attr('id')] )
        {
          candidato.eq(i).find('div.btn_sec_3').css('display', 'none');
          array_eliminados[candidato.eq(i).attr('id')] = jQuery.extend(true, {}, candidato.eq(i).find('div.btn_sec_3') );

        }
        candidato.eq(i).find('div.btn_sec_3').remove();
        // console.log(candidato.eq(i).find('p.disponible'));
        if ( candidato.eq(i).find('p.disponible').length == 0 )
        {
          var disponible = document.createElement('p');
          disponible.className = 'disponible';
          disponible.innerHTML = ' No disponible con la configuracion actual.';
          candidato.eq(i).append(disponible);
        }


        if ( i == 0 ) flag = true;



      }
      else{
        // if ( flag == true && i == 1 )
        // {
        //   var con = 0;
        //   var pos = -1;
        //   for( var j = 0; j < candidato.length; j++ )
        //   {
        //     if( candidato.eq(j).find('div.btn_sec_3').css('display') === 'block' )
        //      {
        //        con = con + 1;
        //        pos = j;
        //      }
        //   }
        //   if ( con == 0 ) candidato.eq(i).find('div.btn_sec_3').css('display', 'block');
        //
        // }
        if( candidato.eq(i).find('div.btn_sec_3').length == 0 )
        {
          // console.log( array_eliminados[candidato.eq(i).attr('id')] );
          candidato.eq(i).find('p.disponible').remove();
          candidato.eq(i).append(array_eliminados[candidato.eq(i).attr('id')]);
          // if ( i == 0 ) candidato.eq(i).find('div.btn_sec_3').css('display', 'block');
          // else candidato.eq(i).find('div.btn_sec_3').css('display', 'none');
        }
        else {
          // array_eliminados[candidato.eq(i).attr('id')] = jQuery.extend(true, {}, candidato.eq(i).find('div.btn_sec_3') );
          // console.log( candidato.eq(i).find('div.btn_sec_3') );
           candidato.eq(i).find('p.disponible').remove();
        }
        currentIndex = ambiant.configuracionGlobal[json_ambiant.RetornaJson(padre*10+1).posc];
        candidato.eq(currentIndex-1).find('div.btn_sec_3').css('display', 'block');
      }
    }

  }


}

function hideContainerInfo(leftContainer) {
  $(leftContainer).find('div.contInfoSlider').hide();
}

function abrirAnimItem(itemInfoContainer,dobleFila) {

  $(itemInfoContainer).find('div').eq(0).show();


  if(dobleFila)
  {
    if(leftContainerPG)
    {
      $(itemInfoContainer).find('div').eq(0).animate({
          height: "140px",
          top: "86px",
        }, 300 );


      $( $(itemInfoContainer).find('div').eq(0)).find('div').eq(0).animate({
          height: "102px",
        }, 300 );

    }
    else
    {
      $(itemInfoContainer).find('div').eq(0).animate({
          height: "120px",
          top: "69.5px",
        }, 300 );


      $( $(itemInfoContainer).find('div').eq(0)).find('div').eq(0).animate({
          height: "92px",
        }, 300 );

    }

  }
  else
  {
    if(leftContainerPG)
    {
      $(itemInfoContainer).find('div').eq(0).animate({
          height: "140px",
          top: "256px",
        }, 300 );


      $( $(itemInfoContainer).find('div').eq(0)).find('div').eq(0).animate({
          height: "102px",
        }, 300 );
    }
    else
    {
      $(itemInfoContainer).find('div').eq(0).animate({
          height: "120px",
          top: "213.5px",
        }, 300 );


      $( $(itemInfoContainer).find('div').eq(0)).find('div').eq(0).animate({
          height: "92px",
        }, 300 );
    }
  }

}

function cerrarAnimItem(itemInfoContainer,item,dobleFila) {
  if(itemInfoContainer !== item)
  {
    $(itemInfoContainer).find('div').eq(0).hide();
  }

  if(dobleFila)
  {
    if(leftContainerPG)
    {
      $(itemInfoContainer).find('div').eq(0).animate({
            height: "38px",
            top: "187px",
      }, 300 );
    }
    else
    {
      $(itemInfoContainer).find('div').eq(0).animate({
            height: "28px",
            top: "161.5px",
      }, 300 );
    }

  }
  else
  {
    if(leftContainerPG)
    {
      $(itemInfoContainer).find('div').eq(0).animate({
            height: "38px",
            top: "358px",
      }, 300 );
    }
    else
    {
      $(itemInfoContainer).find('div').eq(0).animate({
            height: "28px",
            top: "305px",
      }, 300 );
    }

  }

  $( $(itemInfoContainer).find('div').eq(0)).find('div').eq(0).animate({
      height: "0px",
  }, 300 );

}

function bloque() {

  var item;
  var dobleFila6;
  var dobleFila8;

  this.crearBloque= function (extended,titulo,visible,costoExtra,arrayImagenes,arrayCaracteristica,arrayId)
  {
    /*Contenedor Principal*/
    var leftContainer = document.createElement('div');

    if(extended)
    {
      if (leftContainerPG)
        leftContainer.className = "leftContainerExtended leftContainerExtendedPG";
      else
        leftContainer.className = "leftContainerExtended leftContainerExtendedPC";
    }

    else
    {
      if (leftContainerPG)
        leftContainer.className = "leftContainer leftContainerPG";
      else
        leftContainer.className = "leftContainer leftContainerPC";
    }


    leftContainer.addEventListener("mouseenter", function(){
      this.setAttribute( "style", "background: url(" + "\'" + "images/iconos/org_" + json_ambiant.RetornaJson(this.id)['img'] +  " \'); background-repeat: no-repeat ;background-size:100%;");
      showContainerInfo(this);



    });

    leftContainer.addEventListener("mouseleave", function(){
      hideContainerInfo(this);
      if(this.id != $(clickLeftContainer).attr('id'))
        this.setAttribute( "style", "background: url(" + "\'" + "images/iconos/" + json_ambiant.RetornaJson(this.id)['img'] +  " \'); background-repeat: no-repeat;background-size:100%;");
    });

    var contInfoSlider = document.createElement('div');


    //Se verifica el tamaño del array para asignar un tamaño al contenedor de los items
    if(arrayImagenes.length > 1)
    {
      var width;
      var aumento;
      var height;
      //Se define el aumento por item en el contendor segun el tamaño de pantalla y el estilo del contenedor G o C
      if(leftContainerPG)
      {
        contInfoSlider.className = "contInfoSlider contInfoSliderPG";
        aumento=160;
        height=400;
      }

      else
      {
        contInfoSlider.className = "contInfoSlider contInfoSliderPC";
        aumento= 135;
        height = 337;
      }


      if(arrayImagenes.length<=5)
      {
          if(leftContainerPG)
            contInfoSlider.className += " contInfoSliderOnePG";
          else
            contInfoSlider.className += " contInfoSliderOnePC";
          width = arrayImagenes.length * aumento;
      }

      else
      {
        if(leftContainerPG)
          contInfoSlider.className += " contInfoSliderDoblePG";
        else
          contInfoSlider.className += " contInfoSliderDoblePC";

        if(arrayImagenes.length == 6)
        {
          width = 3 * aumento;
          dobleFila6 = true;

        }

        else
        {
          width = 4 * aumento;
          dobleFila8 = true;
        }
        contInfoSlider.style.height = height + 'px' ;

      }
      contInfoSlider.style.width = width.toString() +'px' ;

    }

    //Por defecto el contenedor del bloque esta oculto
    contInfoSlider.style.display = 'none';


    var titleInfoSlider = document.createElement('div');

    //Se Pone un alto para el titulo si es PC
    if(leftContainerPG)
      titleInfoSlider.className = "titleInfoContainer titleInfoContainerPG";
    else
      titleInfoSlider.className = "titleInfoContainer titleInfoContainerPC";


    var titleInfo = document.createElement('div');
    if(leftContainerPG)
      titleInfo.className = "titleInfo titleInfoPG";
    else
      titleInfo.className = "titleInfo titleInfoPC";

    var h1 = document.createElement('h1');
    h1.innerHTML = titulo;

    titleInfo.appendChild(h1);

    var barra_horizontal_bloque_2 = document.createElement('div');
    barra_horizontal_bloque_2.className = "barra_horizontal_bloque_2";
    titleInfo.appendChild(barra_horizontal_bloque_2);

    var img = document.createElement('img');
    if(!leftContainerPG)
    {
      img.style.width="12px";
      img.style.top="-7px";
    }

    var p = document.createElement('p');

    if(visible)
    {
      // img.src = 'img/iconos/cards_visible.svg'
      // p.innerHTML = "Visible en la imagen";
    }
    else
    {
      img.src = 'img/iconos/cards_novisible.svg'
      p.innerHTML = "No visible en la imagen";
    }

    titleInfo.appendChild(img);
    titleInfo.appendChild(p);

    titleInfoSlider.appendChild(titleInfo);

    //Agregamos titleInfoSlider  a contInfoSlider
    contInfoSlider.appendChild(titleInfoSlider);
    //Se manda llamar la funcion que crea los item individuales
    for (var i = 0; i < arrayImagenes.length; i++)
    {
      var itemInfoContainer = this.crearItem(arrayImagenes[i],costoExtra[i],arrayCaracteristica[i],i,arrayId[i],((dobleFila8 && i > 3) || (dobleFila6 && i > 2)) ? 0 : 1 );
      $(itemInfoContainer).find('div').eq(0).find('button').html('Seleccionar ✓');

      //Por defecto se ocultan los items y se verifica si es de tipo costo extra o no
      if($(itemInfoContainer).find("img").length !== 3)
        itemInfoContainer.childNodes[1].style.display = "none";
      else
      {
        //itemInfoContainer.childNodes[0].style.display = "none";
        itemInfoContainer.childNodes[2].style.display = "none";
      }

      //Si es el primer elemento por defecto se muestra el boton
      if(i === 0)
      {
        $(itemInfoContainer).find('div').eq(0).find('button').html('Seleccionado ✓');
        item = itemInfoContainer;
        //si no tiene costo extra solo se muestra el boton
        if($(itemInfoContainer).find("img").length !== 3)
          itemInfoContainer.childNodes[1].style.display = "block";
        else
        {
          //si  tiene costo extra se muestra el boton y su imagen de costo extra
          //itemInfoContainer.childNodes[0].style.display = "block";
          itemInfoContainer.childNodes[2].style.display = "block";
        }

          if(!dobleFila8 && !dobleFila6)
          {
            if($(itemInfoContainer).find("img").length !== 3)
              itemInfoContainer.childNodes[1].className += " bordeIzquierdo";
            else
              itemInfoContainer.childNodes[2].className += " bordeIzquierdo";
          }
        }


      //Si es el ultimo elemento
      if(i === arrayImagenes.length-1)
      {
        if(!dobleFila8 && !dobleFila6)
        {
          if($(itemInfoContainer).find("img").length !== 3)
            itemInfoContainer.childNodes[1].className += " bordeDerecho";
          else
            itemInfoContainer.childNodes[2].className += " bordeDerecho";
        }

      }

      //Si solo hay un elemento
      if(arrayImagenes.length-1 == 0)
      {
        if($(itemInfoContainer).find("img").length !== 3)
          itemInfoContainer.childNodes[1].className += " bordeIzquierdoDerecho";
        else
          itemInfoContainer.childNodes[2].className += " bordeIzquierdoDerecho";
      }

      //Se verifica si es doble fila y se cambian los atributos top y left del boton y la imagen de costo extra si la tiene
      if(dobleFila8 && i > 3)
      {
        if($(itemInfoContainer).find("img").length !== 3)
        {
          if(i == 4)
            itemInfoContainer.childNodes[1].className += " bordeIzquierdo";
          if(i == 7)
            itemInfoContainer.childNodes[1].className += " bordeDerecho";
          if(leftContainerPG)
            itemInfoContainer.childNodes[1].style.top = "358px";
          else
            itemInfoContainer.childNodes[1].style.top = "305.5px";
        }

        else
        {
          if(i == 4)
            itemInfoContainer.childNodes[2].className += " bordeIzquierdo";
          if(i == 7)
            itemInfoContainer.childNodes[2].className += " bordeDerecho";
          //si  tiene costo extra se muestra el boton y su imagen de costo extra
          if(leftContainerPG)
          {
            itemInfoContainer.childNodes[2].style.top = "358px";
            itemInfoContainer.childNodes[0].style.top = "230px";
            itemInfoContainer.childNodes[0].style.left = 130+((i-4)*160) +"px";
          }
          else
          {
            itemInfoContainer.childNodes[2].style.top = "305.5px";
            itemInfoContainer.childNodes[0].style.width = "20px";
            itemInfoContainer.childNodes[0].style.top = "203.5px";
            itemInfoContainer.childNodes[0].style.left = 115+((i-4)*135) +"px";
          }
        }



      }

      if(dobleFila6 && i > 2)
      {

        if($(itemInfoContainer).find("img").length !== 3)
        {
          if(i == 3)
            itemInfoContainer.childNodes[1].className += " bordeIzquierdo";
          if(i == 5)
            itemInfoContainer.childNodes[1].className += " bordeDerecho";
          //Se coloca el boton segun la resolucion
          if(leftContainerPG)
            itemInfoContainer.childNodes[1].style.top = "358px";
          else
            itemInfoContainer.childNodes[1].style.top = "305.5px";
        }

        else
        {
          if(i == 3)
            itemInfoContainer.childNodes[2].className += " bordeIzquierdo";
          if(i == 5)
            itemInfoContainer.childNodes[2].className += " bordeDerecho";

            if(leftContainerPG)
            {
              itemInfoContainer.childNodes[2].style.top = "358px";
              itemInfoContainer.childNodes[0].style.top = "230px";
              itemInfoContainer.childNodes[0].style.left = 130+((i-3)*160) +"px";
            }
            else
            {
              itemInfoContainer.childNodes[2].style.top = "305.5px";
              itemInfoContainer.childNodes[0].style.width = "20px";
              itemInfoContainer.childNodes[0].style.top = "203.5px";
              itemInfoContainer.childNodes[0].style.left = 115+((i-3)*135) +"px";
            }
        }
      }


      contInfoSlider.appendChild(itemInfoContainer);

    }

    //Agregamos contInfoSlider a leftContainer
    leftContainer.appendChild(contInfoSlider);

    return leftContainer;
  }
  this.crearItem = function(imagen,costoExtra,caracterisitca,indice,id,dobleFila)
  {
    //console.log('costoExtra' + costoExtra);
    var itemInfoContainer = document.createElement('div');
    itemInfoContainer.className = "itemInfoContainer";
    //Se pone el tamaño para el item
    if(leftContainerPG)
      itemInfoContainer.className += " itemInfoContainerPG";
    else
      itemInfoContainer.className += " itemInfoContainerPC";

    itemInfoContainer.id = id;

    itemInfoContainer.addEventListener("mouseenter",  function(){
      abrirAnimItem(this,dobleFila);


    });

    itemInfoContainer.addEventListener("mouseleave",function(){
      cerrarAnimItem(this,item,dobleFila);
    });

    itemInfoContainer.addEventListener("mousemove",function(){
      $(item).parent().css('opacity', '1');
    });

    itemInfoContainer.addEventListener("click",  function(){


      //$(item).parent().css('opacity', '0');
      $(item).parent().fadeTo( 50, 0);

      $($(item).parent().parent()).attr("style", "background: url(" + "\'" + "images/iconos/org_" + json_ambiant.RetornaJson($($(item).parent().parent()).attr("id"))['img'] +  " \'); background-repeat: no-repeat;background-size:100%;");
      if(clickLeftContainer != null && $($(item).parent().parent()).attr('id') != $(clickLeftContainer).attr('id') )
        $(clickLeftContainer).attr("style", "background: url(" + "\'" + "images/iconos/" + json_ambiant.RetornaJson($(clickLeftContainer).attr("id"))['img'] +  " \'); background-repeat: no-repeat;background-size:100%;");


      clickLeftContainer = $(item).parent().parent();

      $(item).find('div').eq(0).hide();

      $(item).find('div').eq(0).find('button').html('Seleccionar ✓');
      item = this;
      $(item).find('div').eq(0).find('button').html('Seleccionado ✓');
      //Se muestra el item Seleccionado
      $(item).find('div').eq(0).show();

      //this.id;
      //console.log( this.id );
      ambiant.menu.aplicaOpcion(this.id);
      var mainRender;
      if ( this.id[0] == '3' )
        mainRender = ambiant.renders.Reg_Rend()['Fachada'] ;
      else if (this.id[0] == '4')
        mainRender = ambiant.renders.Reg_Rend()['Sala'] ;
      else if (this.id[0] == '5')
        mainRender = ambiant.renders.Reg_Rend()['Cocina'] ;
      else if (this.id[0] == '6')
        mainRender = ambiant.renders.Reg_Rend()['Bano'] ;
      else if (this.id[0] == '7')
        mainRender = ambiant.renders.Reg_Rend()['Closet'] ;


      //console.log( mainRender );
      // $("#mainRender").css("-webkit-transition", "opacity 0.9s ease-out");
      // $("#mainRender").css("opacity", "0.5");

      $("#mainRender").attr("src", "images/fachadas/" + mainRender );
      // $("#mainRender").css("opacity", "1");
      // $("#mainRender").stop();


    });


    var img = document.createElement('img');
    img.className = "imgItem";
    if(!leftContainerPG)
      img.style.height= "143.5px";

    img.src = imagen;

    var btn_sec_3 = document.createElement('div');
    btn_sec_3.className = "btn_sec_3";

    if(leftContainerPG)
      btn_sec_3.className += " btn_sec_3_PG";
    else
      btn_sec_3.className += " btn_sec_3_PC";


    var btn_plantas = document.createElement('button');
    btn_plantas.className = "btn_Item";

    var infoItemContainer = document.createElement('div');
    infoItemContainer.className = "infoItemContainer";

    if(!leftContainerPG)
    {
      infoItemContainer.style.fontSize =  "12px";
    }


    var infoItem = document.createElement('div');
    infoItem.className = "infoItem";

    //Elementos de infoItem
    var h1 = document.createElement('h1');
    h1.innerHTML = caracterisitca;

    if(!leftContainerPG)
    {
      h1.style.fontSize =  "12px";
    }

    var barra_horizontal_bloque_2 = document.createElement('div');
    barra_horizontal_bloque_2.className = "barra_horizontal_bloque_2";
    barra_horizontal_bloque_2.innerHTML = "____";


    //Agregamos todos los elementos de infoItem
    infoItem.appendChild(h1);
    infoItem.appendChild(barra_horizontal_bloque_2);

    if(costoExtra)
    {
      var imgCEinfoItem = document.createElement('img');
      imgCEinfoItem.className = "imgCEinfoItem";
      imgCEinfoItem.src = "img/iconos/cards_costoextra.svg";

      var imgCEInfoContainer = document.createElement('img');
      if(leftContainerPG)
      {
        imgCEInfoContainer.className = "imgCEInfoContainer imgCEInfoContainerPG";
        imgCEInfoContainer.style.left = 130+(indice*160) +"px";
      }
      else
      {
        imgCEInfoContainer.className = "imgCEInfoContainer imgCEInfoContainerPC";
        imgCEInfoContainer.style.left = 115+(indice*135) +"px";
      }

      imgCEInfoContainer.src = "img/iconos/cards_costoextra.svg";


      itemInfoContainer.appendChild(imgCEInfoContainer);

      var p = document.createElement('p');
      p.innerHTML = "Acabado Plus";


      infoItem.appendChild(imgCEinfoItem);
      infoItem.appendChild(p);
    }

    //Agregamos infoItem a infoItemContainer
    infoItemContainer.appendChild(infoItem);

    //Agregamos infoItemContainer y btn_plantas a btn_sec_3
    btn_sec_3.appendChild(infoItemContainer);
    btn_sec_3.appendChild(btn_plantas);

    //Agregamos imgContainerItem y btn_sec_3 a itemInfoContainer

    itemInfoContainer.appendChild(img);
    itemInfoContainer.appendChild(btn_sec_3);

    return itemInfoContainer;
  }

}

function agregarSlideSec3(id,imagen) {
  var slick = document.createElement('div');

  slick.id = id;
  slick.className = "slicks";
  slick.addEventListener("click", function(){
    // clickSlickPlanta(this);

  });

  var img = document.createElement('img');
  img.className = "imgSliderPlantas";

  img.src = imagen;

  slick.appendChild(img);

  $('#sliderSec3').slick('slickAdd',slick);
}

function showContInfoSlider() {
  if(click != null)
  {
    $(click).parent().show();
  }
}

$(document).ready(function(){

    $('.leftMenuExtended .leftContainer').click(function(ev){
       if(ev.target != this) return false;
       showHideLeftContainerExtended(this);
    });

    $('#sliderSec3').slick({
      dots: false,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3
    });

    var item;
    var block = new bloque();



    $('#btnContinuarBloque_2').click( function(){

      if ( avances[2] == 0 && finalizar == false )
      {
        incrementarAvance(3);
        animacionCargando('mainContainer');
        //Variable para cuando seleccionas un leftContainer se pone en null para que no quede seleccionado
        click=null;
        $('#seccion_3').hide();
        $('#seccion_4').show();


        if ( finalizar == false )
        {
          avances[2] = 1;
          final_ready = 1;

          var leftSlider = document.getElementsByClassName('leftMenuExtended');
          var containers = leftSlider[0].childNodes;
          var pos = 4;
          for( var i = 0; i < containers.length; i++ ) // sacamos sala, cocina, ba~o, closets
          {

            if ( containers[i].nodeName === 'DIV' ) // si es leftContainer
            {
              var img = json_ambiant.RetornaJson( pos )['img'];  // sacamos imagen para el icono en el primer nivel

              containers[i].setAttribute( "style", "background: url(" + "\'" + "images/iconos/" + img +  " \'); background-repeat: no-repeat; background-size:100%;");



              var hijos = containers[i].childNodes;  // buscamos los keyMenu

              for( var h = 0; h < hijos.length; h++)
              {
                if ( hijos[h].nodeName === 'DIV' )  // si es un keyMenu tenemos que agregar un segundo nivel
                {
                  //console.log(hijos[h].childNodes[3]);
                  var sub = ambiant.menu.aplicaOpcion( pos );  // obtenemos las opciones para el segundo nivel
                  // console.log(sub);
                  var lista_img = [];
                  var lista_nom = [];
                  var lista_ids = [];
                  var lista_cos = [];

                  for( var s = 0; s < sub.length; s++)       // por cada icono del segundo nivel hay que agregar sus imagenes
                  {
                    // console.log(sub);
                    var data = json_ambiant.RetornaJson( sub[s].id );
                    var inter = ambiant.menu.aplicaOpcion( sub[s].id );
                    // console.log(data);
                    // console.log(inter);

                    var lista_img = [];
                    var lista_nom = [];
                    var lista_ids = [];
                    var lista_cos = [];
                    var visible = 1;
                    for( var e = 0; e < inter.length; e++ )
                    {
                      var fin =  json_ambiant.RetornaJson( inter[e].id );

                      lista_img.push( "images/jpg/" + fin['img'] );
                      lista_nom.push( fin['nom'] );
                      lista_ids.push( inter[e].id );

                      if ( fin['plus'] === 'true' )
                        lista_cos.push(1);
                      else lista_cos.push(0);

                      if ( fin['rend'] === 'null' ) visible = 0;
                    }


                    var item;
                    var block = new bloque();
                    item = block.crearBloque(1, data['nom'],visible,lista_cos, lista_img, lista_nom, lista_ids);
                    //
                    item.setAttribute( "style", "background: url(" + "\'" + "images/iconos/" + data['img'] +  " \'); background-repeat: no-repeat; background-size:100%;");
                    item.id = sub[s].id;
                    hijos[h].childNodes[3].appendChild(item);
                  }


                }
              }
              pos = pos + 1;
            }

          }

          ambiant.menu.aplicaOpcion(411);
          var mainRender =  ambiant.renders.Reg_Rend()['Sala'] ;


          // console.log( mainRender );
          var loaderImg = new Image(),
              src = "images/fachadas/" + mainRender;

          loaderImg.src = src;

          $(loaderImg).load(function() {
              ocultarAnimacion();
              originalMainRender(screen.width);
              $('#mainRender').attr('src', src);
          });

          $('#btnContinuarBloque_2 .btn_modelo .textoMenuBoton').text("Finalizar");
          $('#btnContinuarBloque_2 .btn_modelo #simbolo').text(" ");



          finalizar = true;
        }
        else if (finalizar == true ) {
          var mainRender =  ambiant.renders.Reg_Rend() ;

          var message =  '{ "nombre":     "michel", \
          "telefono":   "1234567", \
          "correo":     "pollodav@gmail.com", \
          "fracc":  "' + json_ambiant.RetornaJson('11' + ambiant.configuracionGlobal[0])['nom'] + '", \
          "img":    "' + json_ambiant.RetornaJson('11' + ambiant.configuracionGlobal[0])['rend'] + '", \
          "Fachada": "' +   mainRender['Fachada']+'", \
          "modelo": "' + json_ambiant.RetornaJson('21' + ambiant.configuracionGlobal[0])['nom'] + '" \
        }';



        ocultarInfoMenuLeftText();
        finalizarMenu();
        // initWebSocket(message);
        $('#correo').show();
        $('#seccion_3').hide();
        $('#seccion_4').hide();
        $('footer').hide();
      }


    }

    else if ( $('#btnContinuarBloque_2 .btn_modelo .textoMenuBoton').text() == 'Finalizar' )
    {
      ocultarInfoMenuLeftText();
      finalizarMenu();
      $('#bloque_2').hide();
      $('#correo').show();
      $('footer').hide();

      // $("body").css("background", "url(" + "\'" + "images/iconos/config_bg.jpg\'" + ")");
      // $("body").css("background-size", "100%");
      // $("body").css("background-repeat", "no-repeat");

      animacionCargando('mainRender');
      var loaderImg = new Image(),
          src = "images/iconos/config_bg.jpg";
      loaderImg.src = src;

      $(loaderImg).load(function() {
          ocultarAnimacion();
          $('#mainRender').attr('src', src);
      });


    }
    else if (avances[2] == 1 && finalizar == true) {


      incrementarAvance(3);
      $('#bloque_2').show();
      $('#seccion_3').hide();
      $('#seccion_4').show();
      // $('#correo').show();

      // $("#mainRender").attr("src","images/iconos/config_bg.jpg");
      animacionCargando('mainContainer');

      ambiant.menu.aplicaOpcion(411);
      var mainRender =  ambiant.renders.Reg_Rend()['Sala'] ;

      var loaderImg = new Image(),
          src = "images/fachadas/" + mainRender;
      loaderImg.src = src;

      $(loaderImg).load(function() {
          originalMainRender(screen.width);
          ocultarAnimacion();
          $('#mainRender').attr('src', src);
      });

      $('#btnContinuarBloque_2 .btn_modelo .textoMenuBoton').text("Finalizar");
      $('#btnContinuarBloque_2 .btn_modelo #simbolo').text(" ");
      incrementarAvance(3);


    }
    else if ( avances[2] == 0 && finalizar == true)
    {

      incrementarAvance(3);
      $('#bloque_2').show();
      $('#seccion_3').hide();
      $('#seccion_4').show();

      final_ready = 0;
      animacionCargando('mainContainer');

      ambiant.menu.aplicaOpcion(411);
      var mainRender =  ambiant.renders.Reg_Rend()['Sala'] ;

      var loaderImg = new Image(),
          src = "images/fachadas/" + mainRender;
      loaderImg.src = src;

      $(loaderImg).load(function() {
          originalMainRender(screen.width);
          ocultarAnimacion();
          $('#mainRender').attr('src', src);
      });

      $('#btnContinuarBloque_2 .btn_modelo .textoMenuBoton').text("Finalizar");
      $('#btnContinuarBloque_2 .btn_modelo #simbolo').text(" ");
    }
    });
});
