var correoValido=false;
var nombreValido=false;
var despedidaContainer;
$(document).ready(function(){
  var correo = document.getElementById('email');
  correo.addEventListener('blur', function() {
      mailValidator(correo);
  }, true);

  var nombre = document.getElementById('name');
  nombre.addEventListener('blur', function() {
      nameValidator(nombre);
  }, true);


});
function isValidEmail(mail)
{
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail);
}
function mailValidator(input) {
      correoValido=false;
      var error_box = document.getElementById('mail-error-box');
      var text = input.value;

      if (!isValidEmail(text)) {

        input.className='inputError';

        if (!error_box) {
            var caja = document.createElement('div');
            caja.setAttribute("id", "mail-error-box");
            var error = document.createTextNode('Debes escribir un correo valido!');
            caja.appendChild(error);
            input.parentNode.appendChild(caja);
        }
      }
      else {
        correoValido=true;
      }
      if (correoValido) {
        $(input).removeClass('inputError');
        //$(input).css({ boxShadow: 'none' });
        $('#mail-error-box').remove();
      }

}

function nameValidator(input) {
      nombreValido=false;
      var error_box = document.getElementById('name-error-box');
      var text = input.value;
      // console.log(text.length);

      if (text.length == 0) {

        input.className='inputError';

        if (!error_box) {
            var caja = document.createElement('div');
            caja.setAttribute("id", "name-error-box");
            var error = document.createTextNode('Debes escribir un nombre valido!');
            caja.appendChild(error);
            input.parentNode.appendChild(caja);
        }
      }
      else {
        nombreValido=true;
      }
      if (nombreValido) {
        $(input).removeClass('inputError');
        //$(input).css({ boxShadow: 'none' });
        $('#name-error-box').remove();
      }

}
function retornaNomPlantas(planta,mainRender)
{
  // console.log("Planta "+planta)
  // console.log("mainRender[PAlta2]: " + mainRender["PAlta2"] + "   " + mainRender["PFR"])
  var cadena = ""
  switch (planta) {
    case "2 Recámaras":cadena=mainRender["PBaja"]+","+mainRender["PAlta2"];break;
    case "3 Recámaras":cadena=mainRender["PBaja"]+","+mainRender["PAlta3"];break;
    case  "Roof Garden":cadena=mainRender["PBaja"]+","+mainRender["PAlta3"]+","+mainRender["PRG"];break;
    case  "Family Room":cadena=mainRender["PBaja"]+","+mainRender["PAlta3"]+","+mainRender["PFR"];break;
    default:break;
  }//fin  switch
  // console.log(cadena)
  return cadena
}//fin funcion retornaNomPlantas
function enviarCorreo(boton)
{

  if (correoValido && nombreValido)
  {


    var modelo = json_ambiant.RetornaJson('21' + ambiant.configuracionGlobal[1])['nom'];
    if ( modelo == 'Van&#160;Gogh' )
      modelo = "Van Gogh";
    var mainRender =  ambiant.renders.Reg_Rend() ;
    var name = $('#name').val();
    var correo = $('#email').val();
    var tel = $('#telefono').val();
    var msg = $('#message').val();
    var planta = json_ambiant.RetornaJson('22' + ambiant.configuracionGlobal[2])['nom'];
    var plantas = retornaNomPlantas(planta,mainRender)
    var web = 'web';
    var message =  '{ "nombre": "' + name + '"   , \
    "telefono": "'+tel+'"  , \
    "correo": "'+ correo +'"  , \
    "msg": "' + msg+ '"  ,\
    "plantas": "' + plantas+ '"  ,\
    "asesor": "' + web+ '"  ,\
    "fracc":  "' + json_ambiant.RetornaJson('11' + ambiant.configuracionGlobal[0])['nom'] + '", \
    "img":    "' + json_ambiant.RetornaJson('11' + ambiant.configuracionGlobal[0])['rend'] + '", \
    "Fachada": "' +   mainRender['Fachada']+'", \
    "cadena": "' +   ambiant.configuracionGlobal +'", \
    "modelo": "' + modelo + '" \
    }';


    //  console.log(message);

    $("#submit").text("Enviando...");
    $("#submit").attr("onclick", " " );

    initWebSocket(message);
    mostrarDespedida();
  }
  else if(!nombreValido)
  {
    nameValidator(document.getElementById('name'));
  }
  else if(!correoValido) {
    mailValidator(document.getElementById('email'));
  }
}

function ocultarDespedida() {

  $(despedida).remove();
}
function mandaPrincial(){
  window.location.href = "/..";
  // console.log('regresando');
}
function mostrarDespedida()
{

  $("#correo").hide();
  despedidaContainer = document.createElement('div');
  despedidaContainer.className ="despedidaContainer";

  var despedidaInfoContainer = document.createElement('div');
  despedidaInfoContainer.className="despedidaInfoContainer";

  var mensajeDespedida = document.createElement('p');
  mensajeDespedida.innerHTML = "Se ha enviado un mensaje a tu correo. Un asesor se pondrá en contacto contigo.";

  // var botonDespedida = document.createElement('div');

  despedidaInfoContainer.appendChild(mensajeDespedida);

  var divButton = document.createElement('div');
  var buttonDespedida = document.createElement('button');

  divButton.className = "divButton";
  divButton.id = "divButton";
  divButton.onclick = function(){
    window.location.href = "/..";
    // console.log('regresando');
  };
  buttonDespedida.className = "buttonDespedida";

  buttonDespedida.innerHTML = "Finalizar";

  divButton.appendChild(buttonDespedida);
  despedidaInfoContainer.appendChild(divButton);

  despedidaContainer.appendChild(despedidaInfoContainer);


  document.getElementById('mainContainer').appendChild(despedidaContainer);
}
