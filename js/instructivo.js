var isActive=false;
var instructivos=[];
var instrutivoAvance=1;
var firstBackground = [];
var backgroundInicial = [[0,0],["pintores",50],["fachadas",50],["leftMenuExtended",40]];
var instructivoData;
var dataReady=[false,false,false,false];
var botonTitles = ['Continuar','Ahora no','Anterior','Siguiente','Comenzar'];

function dibujarCanvas(aplica,medida,backgroundInstructivo) {
  var top;
  var left;


  var ctx = backgroundInstructivo.getContext('2d');

   switch (aplica) {
     case "mapa":
       top = topMarker-60;
       left = leftMarker;
       instructivos[0].childNodes[3].style.top= topMarker-210 + "px";
       instructivos[0].childNodes[3].style.left= left+ 240 + "px";
       break;
      case "seleccionaTerreno":
        top = $('#seleccionaTerreno').offset().top - 5;
        left = $('#seleccionaTerreno').offset().left +125;
        break;
      case  "pintores":
        top = $('#sliderSec2').offset().top + 42;
        left = $('#sliderSec2').offset().left + 45;
        break;
      case "plantas":
        top = $('#btnContinuarPB').offset().top - 50;
        left = $('#btnContinuarPB').offset().left + 222;
        break;
      case "menuModelo":
        top = $('.menuModelo').offset().top +185;
        left = $('.menuModelo').offset().left +125;
        break;
      case  "fachadas":
        top = $('#311').offset().top + 38;
        left = $('#311').offset().left + 45;
        break;
      case  "leftMenu":
        if(leftContainerPG)
        {
          top = $('#35').offset().top + 29;
          left = $('#35').offset().left + 29;
        }
        else
        {
          top = $('#35').offset().top + 24;
          left = $('#35').offset().left + 24;
        }

        break;
      case  "contInfoExterior":
        if(leftContainerPG)
        {
          top = $('#35').offset().top + 25;
          left = $('#35').offset().left + 235;
        }
        else
        {
          top = $('#35').offset().top + 25;
          left = $('#35').offset().left + 200;
        }

        break;
      case  "leftMenuExtended":
        $(document.getElementById('4')).find('div').eq(0).hide();
        $(document.getElementById('5')).find('div').eq(0).hide();
        $(document.getElementById('6')).find('div').eq(0).hide();
        $(document.getElementById('7')).find('div').eq(0).show();
        if(leftContainerPG)
        {
          top = $('#71').offset().top + 24;
          left = $('#71').offset().left + 24;
        }
        else
        {
          medida = 30;
          top = $('#71').offset().top + 19;
          left = $('#71').offset().left + 19;
        }


        break;
      case  "contInfoInterior":
        if(leftContainerPG)
        {
          top = $('#71').offset().top + 25;
          left = $('#71').offset().left + 235;
        }
        else
        {
          top = $('#71').offset().top + 25;
          left = $('#71').offset().left + 200;
        }

        break;
      default:

   }

  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

  ctx.canvas.width = window.innerWidth;
  ctx.canvas.height = window.innerHeight;

  ctx.fillStyle = "rgba(0,0,0,.6)";
  ctx.rect(0,0,window.innerWidth,window.innerHeight);

  if(medida != 0)
    ctx.arc(left,top,medida,0,2*Math.PI, true);

  ctx.fill();
}
function leerInstructivoData() {

  if(leftContainerPG)
  {

    $.getJSON('js/intructivo.json', function(data){
      instructivoData=data;
      $('#instructivo').find('img').attr("src",'img/topnav_instrucciones_blanco.svg');
      iActive=true;
      mostrarInstructivo(0);
    });
  }
  else
  {

    $.getJSON('js/intructivoPC.json', function(data){
      instructivoData=data;
      $('#instructivo').find('img').attr("src",'img/topnav_instrucciones_blanco.svg');
      iActive=true;
      mostrarInstructivo(0);
    });
  }

}

function inicializarIntructivos(seccion){

    /*
        +Se crea el contenedor principal del tutorial
        +Se crean los items
        +Se inicia la secuencia
    */
    instrutivoAvance=1;
    firstBackground = [];
    var instructivoMainContainer = document.createElement('div');
    instructivoMainContainer.className = 'instructivoMainContainer';

    //Se crea el canvas para el fondo transparente
    var backgroundInstructivo = document.createElement('canvas');
    backgroundInstructivo.className='backgroundInstructivo';

    instructivoMainContainer.appendChild(backgroundInstructivo);
    //Se hace la transformacion al canvas para colorearlo y poder modificarlo despues

    var seccion_ = "seccion_" + (seccion + 1);

    $.each(instructivoData[seccion_], function(index, item){
      var itemInstructivo = crearItemInstructivo(seccion,item.direccion,item.x,item.y,item.info,item.botonTipe,item.circuloTipe,item.actionNext,item.actionBack);
      if(index!=0)
        itemInstructivo.style.display="none";
      else
      {

        dibujarCanvas(backgroundInicial[seccion][0],backgroundInicial[seccion][1],backgroundInstructivo);

      }
      firstBackground.push(item.circuloTipe);
      instructivoMainContainer.appendChild(itemInstructivo);
    });

    instructivos[seccion] =  instructivoMainContainer;
    dataReady[seccion] = true;


}

function mostrarInstructivo(seccion) {
  if(!isActive)
  {
    // console.log('simona');
    inicializarIntructivos(seccion);
    $('body').append(instructivos[seccion]);
    isActive = true;
  }


}

function crearItemInstructivo(seccion,direccion,x,y,info,botonTipe,circuloTipe,actionNext,actionBack) {

  /*
      +Se crea el contenedor para el item
      +Se agrega la clase con la flecha segun la direccion
      +Se crea el intructivoInfo para la mostrar la informacion
      +Se agrega la informacion al intructivoInfo
      +Se crea el contendor intructivoActionsContainer para agregar los botones
      +Se agregan los botones dinamicamente segun botonTipe
      +Se agrega intructivoInfo y intructivoActionsContainer al contendor del item instructivoItemContainer

  */

  var instructivoItemContainer = document.createElement('div');

  //Direccion
  switch (direccion) {
    case 1:
      instructivoItemContainer.className += ' IICtopArrow';
      break;
    case 2:
      instructivoItemContainer.className = ' IICRightArrow';
      break;
    case 3:
      instructivoItemContainer.className = ' IICDownArrow';
      break;
    case 4:
      instructivoItemContainer.className = ' IICLeftArrow';
      break;
    case 5:
      instructivoItemContainer.className = ' IICtopRightArrow';
      break;
    default:

  }

  //Pocision

    $(instructivoItemContainer).css(y[1],y[2]);
    $(instructivoItemContainer).css(x[1],x[2]);
    if(y[0] != 0)
      instructivoItemContainer.style.marginTop = "" + y[0] + "px";
    if(x[0] != 0)
    {
      instructivoItemContainer.style.marginLeft = "" + x[0] + "px";
    }




  var instructivoItem = document.createElement('div');
  instructivoItem.className = 'instructivoItem';

  var intructivoInfo = document.createElement('div');
  if(direccion!=5)
    intructivoInfo.className = 'intructivoInfo';
  else
    intructivoInfo.className = 'intructivoInfoBV';
  intructivoInfo.innerHTML = info;

  var intructivoActionsContainer = document.createElement('div');
  intructivoActionsContainer.className = 'intructivoActionsContainer';

  switch (botonTipe) {
    case 0:
      break;

    case 1:

      var botonDerecho = document.createElement('button');
      botonDerecho.className = 'instructivoBtnAction';
      botonDerecho.style.float = "right";
      botonDerecho.innerHTML = botonTitles[3];
      botonDerecho.addEventListener("click", function(){

        dibujarCanvas(circuloTipe[0],circuloTipe[1],instructivos[seccion].childNodes[0]);
        instructivos[seccion].childNodes[instrutivoAvance].style.display="none";
        instrutivoAvance+=1;
        instructivos[seccion].childNodes[instrutivoAvance].style.display="block";

        switch (actionNext) {
          case 1:
            showContainerInfo( $('#71'));
            break;
          case 2:
            marcarPlanta($('#btnContinuarSegPiso_1').find('button').eq(1));
            abrirAnim($('#btnContinuarSegPiso_1'),1);
            break;
        }
      });

      intructivoActionsContainer.appendChild(botonDerecho);

      break;

    case 2:

      var botonIzquierdo = document.createElement('button');
      botonIzquierdo.className = 'instructivoBtnAction';
      botonIzquierdo.innerHTML = botonTitles[2];

      botonIzquierdo.addEventListener("click", function(){
        instructivos[seccion].childNodes[instrutivoAvance].style.display="none";
        instrutivoAvance-=1;
        instructivos[seccion].childNodes[instrutivoAvance].style.display="block";

        switch (actionBack) {
          case 1:
            dibujarCanvas(backgroundInicial[0][0],backgroundInicial[0][1],instructivos[0].childNodes[0]);
            break;
          case 2:
            dibujarCanvas(backgroundInicial[2][0],backgroundInicial[1][1],instructivos[2].childNodes[0]);
            break;
          case 3:
            dibujarCanvas(firstBackground[0][0],firstBackground[0][1],instructivos[2].childNodes[0]);
            hideContainerInfo( $('#35'));
            break;
          case 4:
            hideContainerInfo(document.getElementById('71'));
            dibujarCanvas(backgroundInicial[3][0],backgroundInicial[3][1],instructivos[3].childNodes[0]);
            break;
          case 5:
            dibujarCanvas(backgroundInicial[1][0],backgroundInicial[1][1],instructivos[1].childNodes[0]);
            marcarPlanta($('#btnContinuarSegPiso_1').find('button').eq(1));
            greenIColor($("#btnContinuarSegPiso_1 "));
            break;
          case 6:
            marcarPlanta($('#btnContinuarSegPiso_1').find('button').eq(1));
            abrirAnim($('#btnContinuarSegPiso_1'),1);

            break;
          default:
        }

      });

      var botonDerecho = document.createElement('button');
      botonDerecho.className = 'instructivoBtnAction';
      botonDerecho.innerHTML = botonTitles[3];

      botonDerecho.addEventListener("click", function(){
        if(circuloTipe[2] != 0)
          dibujarCanvas(circuloTipe[0],circuloTipe[1],instructivos[seccion].childNodes[0]);
        instructivos[seccion].childNodes[instrutivoAvance].style.display="none";
        instrutivoAvance+=1;
        instructivos[seccion].childNodes[instrutivoAvance].style.display="block";

        switch (actionNext) {
          case 1:
            showTerrenoInfo();
            hideSelecciona();
            setMenuInfo(115);
            break;
          case 2:
          showContainerInfo( $('#35'));
            break;
          case 3:
            abrirAnimItem($('#352'),1);
            break;
          case 4:
            abrirAnimItem($('#712'),1);
            break;
          case 5:
            marcarPlanta($('#btnContinuarSegPiso_1').find('button').eq(1));
            abrirAnim($('#btnContinuarSegPiso_1'),1);
            break;
          case 6:
            cerrarAnim($('#btnContinuarSegPiso_1'),1);
          default:
        }
      });

      intructivoActionsContainer.appendChild(botonIzquierdo);
      intructivoActionsContainer.appendChild(botonDerecho);
      break;

    case 3:

      var botonIzquierdo = document.createElement('button');
      botonIzquierdo.className = 'instructivoBtnAction';
      botonIzquierdo.innerHTML = botonTitles[2];

      botonIzquierdo.addEventListener("click", function(){

        instructivos[seccion].childNodes[instrutivoAvance].style.display="none";
        instrutivoAvance-=1;
        instructivos[seccion].childNodes[instrutivoAvance].style.display="block";

        switch (actionBack) {
          case 1:
            dibujarCanvas(firstBackground[1][0],firstBackground[1][1],instructivos[0].childNodes[0]);
            showSelecciona();
            hideInfo();
            break;
          case 2:
            cerrarAnimItem($('#352'),null,1);
            break;
          case 3:
            cerrarAnimItem($('#712'),null,1);
            break;
          case 4:
            dibujarCanvas(firstBackground[1][0],firstBackground[1][1],instructivos[1].childNodes[0]);
            abrirAnim($('#btnContinuarSegPiso_1'),1);
            break;
          default:
        }
      });

      var botonDerecho = document.createElement('button');
      botonDerecho.className = 'instructivoBtnAction';
      botonDerecho.innerHTML = botonTitles[4];

      botonDerecho.addEventListener("click", function(){
        $(instructivos[seccion]).remove();

        switch (actionNext) {
          case 1:
            showSelecciona();
            hideInfo();
            break;
          case 2:
            hideContainerInfo( $('#35'));
            cerrarAnimItem($('#352'),null,1);
            break;
          case 3:
            hideContainerInfo( $('#71'));
            cerrarAnimItem($('#712'),null,1);
            $(document.getElementById('7')).find('div').eq(0).hide();
            break;
          default:

        }
        $('#imgMenuLogo_1').attr("src",'img/topnav_instrucciones.svg');
        iActive=false;
        isActive=false;
      });

      intructivoActionsContainer.appendChild(botonIzquierdo);
      intructivoActionsContainer.appendChild(botonDerecho);

      break;
    case 4:

      var botonIzquierdo = document.createElement('button');
      botonIzquierdo.className = 'instructivoBtnAction';
      botonIzquierdo.innerHTML = botonTitles[0];

      botonIzquierdo.addEventListener("click", function(){

        instructivos[seccion].childNodes[instrutivoAvance].style.display="none";
        instrutivoAvance+=1;
        instructivos[seccion].childNodes[instrutivoAvance].style.display="block";

      });

      var botonDerecho = document.createElement('button');
      botonDerecho.className = 'instructivoBtnAction';
      botonDerecho.innerHTML = botonTitles[1];

      botonDerecho.addEventListener("click", function(){
        $(instructivos[seccion]).remove();
        isActive=false;
        $('#imgMenuLogo_1').attr("src",'img/topnav_instrucciones.svg');
        iActive=false;
      });
      intructivoActionsContainer.innerHTML = "<p>¿Deseas continuar el tutorial?<p>"
      intructivoActionsContainer.appendChild(botonIzquierdo);
      intructivoActionsContainer.appendChild(botonDerecho);

      break;

    default:

  }

  if(direccion != 5)
  {
    var botonCerrar = document.createElement('button');
    botonCerrar.className = 'instructivoBtnCerrar';

    botonCerrar.addEventListener("click", function(){

      $(instructivos[seccion]).remove();
      isActive=false;

      if(indicePrecionado == 3)
        $(document.getElementById('7')).find('div').eq(0).hide();
      if(indicePrecionado == 1)
      {
        if(instrutivoAvance == 2)
        {
          marcarPlanta($('#btnContinuarSegPiso_1').find('button').eq(1));
          greenIColor($('#btnContinuarSegPiso_1'));
        }
        if(instrutivoAvance == 3)
          cerrarAnim($('#btnContinuarSegPiso_1'),1);
      }

      $('#imgMenuLogo_1').attr("src",'img/topnav_instrucciones.svg');
      iActive=false;

    });

    instructivoItem.appendChild(botonCerrar);
  }




  instructivoItem.appendChild(intructivoInfo);
  instructivoItem.appendChild(intructivoActionsContainer);

  instructivoItemContainer.appendChild(instructivoItem);

  return instructivoItemContainer;
}

function actualizarPocision() {

  if(dataReady[indicePrecionado])
  {
    switch (instrutivoAvance) {
      case 1:
        dibujarCanvas(backgroundInicial[indicePrecionado][0],backgroundInicial[indicePrecionado][1],instructivos[indicePrecionado].childNodes[0]);
        break;
      case 2:
        dibujarCanvas(firstBackground[0][0],firstBackground[0][1],instructivos[indicePrecionado].childNodes[0]);
        break;
      case 3:
        dibujarCanvas(firstBackground[1][0],firstBackground[1][1],instructivos[indicePrecionado].childNodes[0]);
        break;
      case 4:
        dibujarCanvas(firstBackground[2][0],firstBackground[2][1],instructivos[indicePrecionado].childNodes[0]);
        break;
      default:

    }
  }
}
window.onresize= actualizarPocision;
