$(document).ready(function(){

  var rightNavSplitHidden = true;
  var secModeloHidden = true;
  var secCrecimientoHidden = true;
  var currentBtn;



  //Boton fraccionamiento con submenus//

    $('#btnCocina').click(function() {
      /*
      console.log('cocina');
      /* SE CREA OBJETO MenuJ0 Y SE INICIALIZA EL PROCESO DE SELECCIÓN

      var menuJ0 = new MenuJ0("opciones.txt","js/cocina.txt");
      menuJ0.archivoMenu = "js/opciones.txt";
      menuJ0.archivoConfiguraciones = "js/cocina.txt";
      menuJ0.prepare("js/opciones.txt", "js/cocina.txt");

      menuJ0.señal.add(onEnd); //add listener

      function onEnd(){
          var ops = menuJ0.menus[0].show(menuJ0.configuraciones, menuJ0.configuracion, "");
            $('#rightNavSplit').append("<button id='1'  class='rightNavButton' name='btn1'>Hola</button>");
      }
      */


    });

  $('#btnFrac').click(function() {
    if(rightNavSplitHidden)
    {

      $('#rightNavSplit').show();
      $(this).css({ opacity: 1 });
      rightNavSplitHidden=false;
      $(currentBtn).off("mouseleave");
    }

    else
    {
      $('#rightNavSplit').hide(ocultarMenus);
      $(this).css({ opacity: 0.6 });
      rightNavSplitHidden=true;
      $(currentBtn).mouseleave(function() {
            $(this).css({ opacity: .6 });

      });
    }

  });

  $('#btnMap').click(function(){
    //$(currentBtn).off("mouseleave");
    var mapOptions = {
      zoom: 8,
      center: new google.maps.LatLng(40.417181, -3.700823),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    };

    if(!$('#mapaContainer').length)
    {
      $('#mainContainer').append("<div id='mapaContainer' class='seccion'></div>");
      var myMap = new google.maps.Map(document.getElementById("mapaContainer"),mapOptions);

      var marker = new MarkerWithLabel({
        position: myMap.getCenter(),
        icon: {
          path: google.maps.SymbolPath.CIRCLE,
          scale: 0, //tamaño 0
        },
        map: myMap,
        draggable: true,
        labelAnchor: new google.maps.Point(20, 20),
        labelClass: "label", // the CSS class for the label
      });
    }
    else
    {
      $('#mapaContainer').remove();
    }

  });
  //Funcion para ocultar menus y devolverlos a su estado original//
  function ocultarMenus() {
      //Ocultar Submenus//
      $('#secCrecimiento').hide(200);
      $('#secModelo').hide(200);
      //Devolver valores a su funcion original//
      secCrecimientoHidden = true;
      secModeloHidden = true;
      $('#rightNavSplitBtn1').css({ opacity: .6 });
      $('#rightNavSplitBtn1').mouseleave(function() {
            $(this).css({ opacity: .6 });

      });
      $('#rightNavSplitBtn2').css({ opacity: .6 });
      $('#rightNavSplitBtn2').mouseleave(function() {
            $(this).css({ opacity: .6 });

      });


  }


  $('.rightNavButton').hover(function() {
        $(this).css({ opacity: 1 });
        currentBtn = this;

  });


  $('.rightNavButton').mouseleave(function() {
        $(this).css({ opacity: .6 });

  });


  $('#rightNavSplitBtn1').click(function() {
    if(secModeloHidden)
    {
      if(!secCrecimientoHidden)
      {
        $('#secCrecimiento').hide();
        secCrecimientoHidden = true;
        $('#rightNavSplitBtn2').css({ opacity: .6 });
        $('#rightNavSplitBtn2').mouseleave(function() {
              $(this).css({ opacity: .6 });

        });
      }
      $('#secModelo').show();
      secModeloHidden = false;
      $(currentBtn).off("mouseleave");
    }

    else
    {

      $('#secModelo').hide();
      secModeloHidden = true;
      $(currentBtn).mouseleave(function() {
            $(this).css({ opacity: .6 });

      });
    }
  });

  $('#rightNavSplitBtn2').click(function() {

    console.log(secCrecimientoHidden);

    if(secCrecimientoHidden)
    {

      if(!secModeloHidden)
      {
        $('#secModelo').hide();
        secModeloHidden = true;
        $('#rightNavSplitBtn1').css({ opacity: .6 });
        $('#rightNavSplitBtn1').mouseleave(function() {
              $(this).css({ opacity: .6 });

        });
      }

      $('#secCrecimiento').show();
      secCrecimientoHidden = false;
      $(currentBtn).off("mouseleave");

    }

    else
    {

      $('#secCrecimiento').hide();
      secCrecimientoHidden = true;
      $(currentBtn).mouseleave(function() {
            $(this).css({ opacity: .6 });

      });
    }
  });

  //Generar pantalla completa //
  $('#btnCrecimiento').click(function(){
    $('#mainContainer').append("<div id='pantallaCompletaContainer'><div alt='Prueba' id='imagenPantallaCompleta'><button id='botonCerrar' type='btnCerrar' name='btnCerrar'></button></div></div>");

    $('#botonCerrar').click(function(){
      $('#pantallaCompletaContainer').remove();
    });

    $('#pantallaCompletaContainer').click(function(){
      $(this).remove();
    });
  });

});
