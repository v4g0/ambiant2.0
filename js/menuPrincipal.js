var indiceAvance;
var indicePrecionado;
var opciones = ['Fraccionamiento','Modelos','Exteriores','Interiores'];
var iActive=false;
var ayuda = false;

function inicializarMenu(){
  /*
      +Inicializa los valores de el indice de avance y el indice precionado
      +Al primer elemento se le pone un background white y un box-shadow white
      +Se muestra su circulo
  */

  indiceAvance = 0;
  indicePrecionado = 0;
  $('div.menuSec').eq(indicePrecionado).css( "background", "white" );
  $( ".menuSec" ).eq(indicePrecionado).css('box-shadow', '0px 1px 0px white');
  $('div.menuSec').eq(indicePrecionado).find('div').show();
}
function finalizarMenu() {
  $( ".menuSec" ).attr( "onclick", " " );
  $( ".menuSec" ).attr( "onmouseenter", " " );
  $( ".menuSec" ).attr( "onmouseleave", " " );
  $( "#instructivo" ).unbind("click");
  $('div.menuSec').eq(indicePrecionado).find('div').hide();

}
function clicMenuSec(seccion) {
  /*
    menuSecClicked = index;
    seccion.style.background = 'white';
    seccion.style.boxShadow =  "0px 1px 0px white";
  */
  /*
      +Si la seccion precionada es menor o igual al indiceAvance y la seccion es diferente del indicePrecionado, el indicePrecionado sera igual a la seccion
      +Se ejecuta un caso diferente segun el indice precionado
  */
  if(!isActive)
  {
    if(seccion <= indiceAvance &&  seccion != indicePrecionado)
    {
      indicePrecionado = seccion;
      switch (indicePrecionado) {
        case 0:
          originalMainRender(screen.width);
          $('footer').hide();
          $('#bloque_1').show();
          $('#seccion_1').show();
          $('#seccion_2').hide();
          $('#bloque_2').hide();
          break;
        case 1:
          originalMainRender(screen.width);
          $('footer').show();
          $('#bloque_1').show();
          $('#seccion_1').hide();
          $('#seccion_2').show();
          $('#bloque_2').hide();
          animacionCargando('mainRender');
          var loaderImg = new Image(),
              src = "images/iconos/config_bg.jpg";
          loaderImg.src = src;

          $(loaderImg).load(function() {
              ocultarAnimacion();
              $('#mainRender').show();
              $('#mainRender').attr('src', src);
          });
          break;
        case 2:
          resizeMainRender(screen.width,screen.height);
          $('#seccion_1').hide();
          $('#seccion_2').hide();
          $('#bloque_2').show();
          $('#seccion_3').show();
          $('#seccion_4').hide();
          ambiant.menu.aplicaOpcion(311);
          var mainRender = ambiant.renders.Reg_Rend()['Fachada'] ;
          $("#mainRender").attr("src", "images/fachadas/" + mainRender );

          $('#btnContinuarBloque_2 .btn_modelo .textoMenuBoton').text("Continuar");
          $('#btnContinuarBloque_2 .btn_modelo #simbolo').text("›");
          break;
        case 3:
          originalMainRender(screen.width);
          $('#seccion_1').hide();
          $('#seccion_2').hide();
          $('#bloque_2').show();
          $('#seccion_3').hide();
          $('#seccion_4').show();

          ambiant.menu.aplicaOpcion(411);
          // console.log( ambiant.menu.aplicaOpcion(65) );
          $('#btnContinuarBloque_2 .btn_modelo .textoMenuBoton').text("Finalizar");
          $('#btnContinuarBloque_2 .btn_modelo #simbolo').text(" ");
          var mainRender = ambiant.renders.Reg_Rend()['Sala'] ;
          $("#mainRender").attr("src", "images/fachadas/" + mainRender );
          final_ready = 1;
          break;

        default:

      }
    }
  }

}
function menuSecMouseenter(seccion) {
  /*
      +Pone a todos los elementos de la clase .menuSec color #d45b00 exepto al que se hace mouseenter
      +Pone a todos los elementos de la clase .menuSec boxShadow #ff8e14
      +Pone al elemento del mouseenter en en color #ff8e14
      +Muestra su ciruclo del elemento del mouseenter
      +Cambia el texto por el elemento del mouseenter
  */
  $('div.menuSec:not(:eq('+seccion+'))').css( "background", "#d45b00" );
  $('div.menuSec').css('box-shadow', '0px 1px 0px #ff8e14');
  $('div.menuSec').eq(seccion).css( "background", "#ff8e14" );
  $('div.menuSec').eq(seccion).find('div').show();
  $( "#infoMenuLeftText h1" ).text(opciones[seccion]);


}
function menuSecMouseleave(seccion) {
  /*
      +Se pone el elemento del mouseleave en el color #d45b00
      +Se oculta su circulo
      +Se ponen el elemento precionado y todos a su derecha en blanco
      +Se muestra el circulo del elemento precionado por si el mismo se hizo mouseleave
      +Se regresa el texto que se cambio al del elemento precionado
  */
  $('div.menuSec').eq(seccion).css( "background", "#d45b00" );
  $('div.menuSec').eq(seccion).find('div').hide();

  $('div.menuSec:not(:eq('+seccion+'))').find('div').hide();
  $('div.menuSec:lt('+indicePrecionado +')').css( "background", "white" );
  $('div.menuSec:lt('+indicePrecionado +')').css('box-shadow', '0px 1px 0px white');
  $('div.menuSec').eq(indicePrecionado).find('div').show();
  $('div.menuSec').eq(indicePrecionado).css( "background", "white" );
  $('div.menuSec').eq(indicePrecionado).css('box-shadow', '0px 1px 0px white');
  $( "#infoMenuLeftText h1" ).text(opciones[indicePrecionado]);
}
function ocultarInfoMenuLeftText() {
  $('#infoMenuLeftText').hide();
}
function incrementarAvance(indice) {
  if(indice>indiceAvance)
    indiceAvance=indice;

  indicePrecionado = indice;
  $('div.menuSec').eq(indicePrecionado-1).find('div').hide();
  $('div.menuSec').eq(indicePrecionado).find('div').show();
  $( "#infoMenuLeftText h1" ).text(opciones[indicePrecionado]);
  $('div.menuSec').eq(indicePrecionado).css( "background", "white" );
  $('div.menuSec').eq(indicePrecionado).css('box-shadow', '0px 1px 0px white');


}
$(document).ready(function(){

  $('#instructivo').click(function() {
    $(this).find('img').attr("src",'img/topnav_instrucciones_blanco.svg');
    iActive=true;
    mostrarInstructivo(indicePrecionado);
  });

  $('#ayuda').click(function() {
    if(ayuda)
    {
      ayuda= false;
    }
    else
    {
      ayuda= true;
    }


  });


  $('#home').click(function() {
    window.location.href = "/..";
  });

  $('.iconoInfoMenuContainer').mouseenter(function functionName() {
    var id = $(this).find('img').attr('id');
    $( "#infoMenuLeftText h1" ).hide();
    if(id == 'imgMenuLogo_1')
    {
      $(this).find('img').attr("src",'img/topnav_instrucciones_blanco.svg');
      $(this).animate({
          width: "85px",
        }, 200 );
      $(this).find('h1').show();

    }
    else if(id == 'imgMenuLogo_2')
    {
      $(this).find('img').attr("src",'img/topnav_asesoria_blanco.svg');
      $(this).animate({
          width: "60px",
        }, 200 );
        $(this).find('h1').show();
    }
    else
    {
      $(this).find('img').attr("src",'img/topnav_home_blanco.svg');
      $(this).animate({
          width: "60px",
        }, 200 );
        $(this).find('h1').show();
    }

  });

  $('.iconoInfoMenuContainer').mouseleave(function functionName() {
    var id = $(this).find('img').attr('id');
    $( "#infoMenuLeftText h1" ).show();
    if(id == 'imgMenuLogo_1')
    {
      if(!isActive)
        $(this).find('img').attr("src",'img/topnav_instrucciones.svg');

      $(this).find('h1').hide();
      $(this).animate({
          width: "20px",
        }, 200 );
    }
    else if(id== 'imgMenuLogo_2')
    {
      $(this).find('img').attr("src",'img/topnav_asesoria.svg');
      $(this).animate({
          width: "20px",
        }, 200 );
      $(this).find('h1').hide();
    }
    else
    {
      $(this).find('img').attr("src",'img/topnav_home.svg');
      $(this).animate({
          width: "20px",
        }, 200 );
      $(this).find('h1').hide();
    }

  });

});
